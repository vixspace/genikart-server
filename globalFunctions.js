const fs = require("fs")
const AWS = require('aws-sdk')
const path = require('path');
const axios = require('axios')
const { verify } = require("crypto");
const ObjectID = require('mongodb').ObjectID;
const MSG91 = require('msg91');
let msg91 = MSG91(process.env.MSG91_AUTH_KEY,process.env.MSG91_SENDER_ID,4);
const WalletLog = require('./models/walletLog')
const { ObjectId } = require('mongodb');
const { admin } = require('./firebase');


const avaiblesFileTypes = ['jpeg','jpg','gif','mp4','png']


validatePhone = function(phone){
    //validatePhone for validating the phoneNumber
    let re = /^(\+\d{3})?\d{10}$/;
    return re.test(phone.replace(/\s+/g, ''));
}

verifyObjectId = (Id) => {
  if(ObjectID.isValid(Id))
    return true
  else
    return false
}

sendLoginOtp = () => {
}

generateRandomString = function (length = 20) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
  return '12345'
}
generateRandomNumber = function (length = 20) {
  var result           = '';
  var characters       = '0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
  return '12345'
}




 

generateVerificationCode = function(min = 1000, max = 9999) {
  return "1234";
  return Math.floor(Math.random() * (max - min) + min);
}


TE = function (err, code,log) {
  // TE stands for Throw Error, showing error in development mode only
  let _err;
  switch (true) {
    case typeof err.code === "number" && err.code === 11000:
      _err = "Record already exist.";
      break;
    default:
      _err = err;
      break;
  }
  if (process.env.NODE_ENV === "development") {
    console.error(log)
  }
  throw new Error(_err,code);
};



ReE = function (res, err, code, log) {
  // Error Web Response
  //showing log in development mode only
  if (process.env.NODE_ENV === "development") {
    console.error(`Error logged from API :${log}`);
  }
  let send_data = { success: false };
  if (typeof code !== "undefined") res.statusCode = code;

  if (err instanceof Error && typeof err.message != "undefined") {
    err = err.message;
  } else {
    send_data = { ...send_data, ...err }; //merge the objects
    return res.json(send_data);
  }
  console.log(err)
  return res.json({ success: false, message: err },code);
};

ReS = function (res, data, code) {
  // Success Web Response
  let send_data = { success: true };
  if (typeof data === "object") {
    send_data = Object.assign(data, send_data); //merge the objects
  }

  if (typeof code !== "undefined") res.statusCode = code;

  return res.json(send_data);
};


const notification_options = {
  priority: "high",
  timeToLive: 60 * 60 * 24
};

sendNotification = async (fcm_token,message) => {
  const data = await admin.messaging().sendToDevice(fcm_token, message, notification_options)
  return data
}

ImageUpload = async (file) => {
    let [base,ext] = getExtensions(file.originalFilename)

    // Read in the file, convert it to base64, store to S3
    s3 = new AWS.S3({
      accessKeyId: process.env.ACCESS_KEY_ID,
      secretAccessKey: process.env.ACCESS_SECRET_KEY,
    });
    return await s3.upload({
        Bucket: process.env.BUCKET_NAME,
        Key: generateRandomString()+path.extname( file.path ),
        Body: fs.createReadStream(file.path),
        ACL: 'public-read',
    }).promise();
};

getExtensions = (name) => {
  name = name.trim()
  const match = name.match(/^(\.+)/)
  let prefix = ''
  if (match) {
    prefix = match[0]
    name = name.replace(prefix, '')
  }
  const index = name.indexOf('.')
  const ext = name.substring(index + 1)
  const base = name.substring(0, index) || ext
  return [prefix + base, base === ext ? '' : ext]
}

makeInstance = async(model,data,id) => {
  let instance;
  if(id == null){
      instance = new model;
  }else{
      instance = await model.findById(id);
      if(instance == null){
          return TE('Cannot Found Instance')
      }
  }
  Object.keys(data).map((key,) => {
      instance[key] = data[key];
  })
  return await instance.save();
}

alreadyExistTitle = async(model,name) => {
  return await model.find({name:name}).countDocuments();
}

alreadyExist = async(model,query) => {
  return await model.find(query).countDocuments();
}

walletFinalAmount = async(user_id) => {
  const total_log = await WalletLog.find({user_id}).countDocuments();
  if(total_log == 0){
      return 0;
  }
  const data = await WalletLog

          .aggregate([
              { $match: {
                  user_id: ObjectId(user_id)
              }},
              { $group: {
                  _id: null,
                  total:       { $sum: { $subtract: ["$credit", "$debit"] } },
              }}
          ]);
  return data[0].total
}