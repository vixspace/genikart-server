const Category = require('../../models/category')
const SubCategory = require('../../models/subCategory')
const City = require('../../models/city')
const HeathCondition = require('../../models/heath_condition');
const Product = require('../../models/product');

module.exports = (function () {
    this.create = async({body}) => {
        const {name,imageArray,pharmaCompanyId,about,
                composition,quantity,max_retail_price,selling_price,
                expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
                weight,width,length,status} = body;
        if(!composition || !cityId || !expiry_date || !imageArray ||  !quantity || !max_retail_price || !selling_price || !name || !pharmaCompanyId || !categoryId || !subCategoryId || !heathConditionId || !weight || !width || !length){
            return TE("Invalid Parameter: name,imageArray,pharmaCompanyId,about,composition,quantity,max_retail_price,selling_price,expiry_date,cityId,categoryId,subCategoryId,heathConditionId,weight,width,length,status is required");
        }
      
        // check if title Exists
        if(!await alreadyExist(Category,{_id:categoryId})){
            return TE("Category Does not Already Exists");
        }
        // check if Subtitle Exists
        if(!await alreadyExist(SubCategory,{category_id:categoryId,_id:subCategoryId})){
            return TE("Sub Category Does not Already Exists");
        }
        // cityId
        if(!await alreadyExist(City,{_id:cityId})){
            return TE("City Does not Already Exists");
        }

        // check if title Exists
        if(!await alreadyExist(HeathCondition,{_id:heathConditionId})){
            return TE("Heath Condition Does not Already Exists");
        }

        const data = {name,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status}
        const instance = await makeInstance(Product,data,null);
        return {
            data: {
                message:"New Product Added",
                ProductInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {name,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status,product_id} = body;
        if(!composition || !cityId || !expiry_date || !imageArray ||  !quantity || !max_retail_price || !selling_price || !name || !pharmaCompanyId || !categoryId || !subCategoryId || !heathConditionId || !weight || !width || !length || !product_id){
            return TE("Invalid Parameter: name,imageArray,pharmaCompanyId,about,composition,quantity,max_retail_price,selling_price,expiry_date,cityId,categoryId,subCategoryId,heathConditionId,weight,width,length,status is required");
        }
    
        // check if product Exists
        if(!await alreadyExist(Product,{_id:product_id})){
            return TE("Category Does not Already Exists");
        }
        // check if title Exists
        if(!await alreadyExist(Category,{_id:categoryId})){
            return TE("Category Does not Already Exists");
        }
        // check if Subtitle Exists
        if(!await alreadyExist(SubCategory,{category_id:categoryId,_id:subCategoryId})){
            return TE("Sub Category Does not Already Exists");
        }
        // cityId
        if(!await alreadyExist(City,{_id:cityId})){
            return TE("City Does not Already Exists");
        }

        // check if title Exists
        if(!await alreadyExist(HeathCondition,{_id:heathConditionId})){
            return TE("Heath Condition Does not Already Exists");
        }

        const data = {name,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status}
        const instance = await makeInstance(Product,data,product_id);
        return {
            data: {
                message:"New Product Updated",
                ProductInstance:instance
            }
        }
    }
    this.getAllView = async({body}) => {
        const {name,categoryId,subCategoryId,heathConditionId,heathConditionName,skip,limit,pharmaCompanyId} = body;
        const searchParams = {};
        searchParams['status'] = true;
        if(pharmaCompanyId != "" && pharmaCompanyId != undefined){
            searchParams['pharmaCompanyId'] = pharmaCompanyId
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        if(categoryId != "" && categoryId != undefined){
            searchParams['categoryId'] = { "$in": categoryId }
        }
        if(subCategoryId != "" && subCategoryId != undefined){
            searchParams['subCategoryId'] = { "$in": subCategoryId }
        }
        if(heathConditionId != "" && heathConditionId != undefined){
            searchParams['heathConditionId'] = { "$in": heathConditionId }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        const all_data = await Product.find(searchParams).populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'}).sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Product",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,name,categoryId,subCategoryId,heathConditionId,heathConditionName,skip,limit,pharmaCompanyId} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(pharmaCompanyId != "" && pharmaCompanyId != undefined){
            searchParams['pharmaCompanyId'] = pharmaCompanyId
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        if(categoryId != "" && categoryId != undefined){
            searchParams['categoryId'] = { "$in": categoryId }
        }
        if(subCategoryId != "" && subCategoryId != undefined){
            searchParams['subCategoryId'] = { "$in": subCategoryId }
        }
        if(heathConditionId != "" && heathConditionId != undefined){
            searchParams['heathConditionId'] = { "$in": heathConditionId }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        const all_data = await Product.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Product",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const product = await Product.findById(id)
        if(product == null) {
            return TE('Cannot Find Product');
        }
        await product.delete();
        return {
            data:{
                message:"Product Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {product_id,status} = body
        if(!product_id && status != undefined){
            return TE("Invalid Parameter: product_id,status is required");
        }
        // check if product Exists
        if(!await alreadyExist(Product,{_id:product_id})){
            return TE("Product Does not Already Exists");
        }
        const data = {status}
        const instance = await makeInstance(Product,data,product_id);
        return {
            data: {
                message:"Product Status Updated",
                ProductInstance:instance
            }
        }
    }
    this.getIndividualProduct = async({params}) => {
        const {id} = params;
        // check if product Exists
        if(!await alreadyExist(Product,{_id:id})){
            return TE("Product Does not Already Exists");
        }
        const product = await Product.findById(id).populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'})
        return {
            data:{
                message:"Individual Product",
                product
            }
        }
    }
    return this;
  })();