const User = require('../../models/user')
const Image = require('../../models/image')


module.exports = (function () {
    /**
     * @params phoneNumber
     * @For send Login Otp
     */
    this.loginSendOtp = async ({body}) => {
        const {phoneNumber} = body
        const verificationCode = generateVerificationCode();

        if(!phoneNumber){
            throw new Error("Cannot find phoneNumber")
        }

        let isExist = await User.findOne({phoneNumber});
        if(isExist == null){
            //create new 
            const user = await User.create({phoneNumber})
            if(user == null){
                TE("Error occured while creating new user")
            }
        }
        // update the user with the new verificationCode
        let updateUserWithOtp = await User.updateOne({phoneNumber},{verificationCode})
        await sendLoginOtp(phoneNumber,verificationCode);
        if(updateUserWithOtp.nModified != 1)
            TE("Error error in send Otp process")

        return {
            data:{
                message: 'Otp Sent successfully'
            }
        }
    }



    this.upload = async({body,files}) => {
        const {file} = files
        if(!file){
            TE("Cannot Found file in body");
        }
        
        const response = await ImageUpload(file);
        const newImage = await Image.create({
            path:response.Location,
            name:file.originalFilename
        });
        return {
            data:{
                image:newImage
            }
        }
    }
    return this;
  })();