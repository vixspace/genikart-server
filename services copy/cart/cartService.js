const Product = require('../../models/product');
const Cart = require('../../models/cart');

module.exports = (function () {
    
    
    this.current_cart_items = async (user_id) => {
        return await Cart.find({
            user_id,in_cart:true
        }).populate('product_id').populate({path:'product_id',populate:'imageArray'}).sort({'updatedAt':1}).select('-in_cart -status')
    }
    this.addUpdateToCart = async({body,decoded}) => {
        const {
            product_id,quantity 
        } = body
        const user_id = decoded._id;
        if(!product_id || !quantity || !user_id){
            return TE('Invalid Parameter: product_id, quantity is required');
        }
        // check if product is avaible
        if(!await alreadyExist(Product,{status:true,_id:product_id})){
            return TE('Product is not avaible');
        }
        const product = await Product.findById(product_id).select('quantity')
        if(product.quantity < quantity){
            return TE('Product Quantity not avaible');
        }
        const cart = await Cart.findOneAndUpdate({product_id,user_id,in_cart:true},{$set:{quantity:quantity}},{new: true,upsert: true,});
        const all_cart_items = await current_cart_items(user_id)
        return {
            data:{
                updatedItem:cart,
                all_cart_items,
                total_item:all_cart_items.length
            }
        }
    }


    this.getCartItem = async({decoded}) => {
        const user_id = decoded._id;
        const all_cart_items = await current_cart_items(user_id)
        return {
            data:{
                all_cart_items,
                total_item:all_cart_items.length
            }
        }
    }
        
    return this;
  })();