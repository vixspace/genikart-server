const Category = require('../../models/category')
const HeathCondition = require('../../models/heath_condition');


module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,logo,status} = body;
        if(!s_no || !name ){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        
        // check if title Exists
        if(await alreadyExist(HeathCondition,{s_no:s_no})){
            return TE("Heath Condition S No. Already Exists");
        }

        // check if title Exists
        if(await alreadyExist(HeathCondition,{name})){
            return TE("Heath Condition Name Already Exists");
        }
        

        const data = {s_no,name,logo,status}
        const instance = await makeInstance(HeathCondition,data,null);
        return {
            data: {
                message:"New Heath Condition Added",
                HeathConditionInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {s_no,name,logo,status,heath_condition_id} = body;
        if(!s_no || !name || !logo || !heath_condition_id){
            return TE("Invalid Parameter: s_no,name,logo,heath_condition_id is required");
        }
        const data = {s_no,name,logo,status}
        const instance = await makeInstance(HeathCondition,data,heath_condition_id);
        return {
            data: {
                message:" Updated",
                HeathConditionInstance:instance
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,name,skip,limit} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i',);
        }
        const all_data = await HeathCondition.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Heath Condition",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const heath_condition = await HeathCondition.findById(id)
        if(heath_condition == null) {
            return TE('Cannot Find HeathCondition');
        }
        await heath_condition.delete();
        return {
            data:{
                message:"Heath Condition Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {heath_condition_id,status} = body
        if(!heath_condition_id && status != undefined){
            return TE("Invalid Parameter: heath_condition_id,status is required");
        }
        const data = {status}
        const instance = await makeInstance(HeathCondition,data,heath_condition_id);
        return {
            data: {
                message:"Heath Condition Status Updated",
                HeathConditionInstance:instance
            }
        }
    }
    return this;
  })();