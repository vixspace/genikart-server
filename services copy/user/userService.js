const Category = require('../../models/category')
const User = require('../../models/user')
const Address = require('../../models/address')
const UserOtp = require('../../models/userOtp')
const jwt = require("jsonwebtoken");
const { plugin } = require('mongoose');



module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,logo,status} = body;
        if(!s_no || !name ){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        // check if title Exists
        if(await alreadyExistTitle(Category,name)){
            return TE("Category Already Exists");
        }

        if(await alreadyExist(Category,{s_no:s_no})){
            return TE("City S_No Already Exists");
        }

        const data = {s_no,name,logo,status}
        const instance = await makeInstance(Category,data,null);
        return {
            data: {
                message:"New Category Added",
                categoryInstance:instance
            }
        }
    }

    this.sendOtp = async({body}) => {
        const {phoneNumber} = body;
        if(!phoneNumber && !validatePhone(phoneNumber)){
            return TE("Invalid Parameter: phonenumbe is required");
        }
        const OTP = generateVerificationCode();

        let instance_data = {
            phoneNumber,
            OTP
        }
        const user_otp_instance = await makeInstance(UserOtp,instance_data);
        if(user_otp_instance == null){
            return TE("Issue in generating OTP. Please try again later.");
        }
        await sendLoginOtp(user_otp_instance);
        return {
            data:{
                message:"Otp Sended successfully"
            }
        }
    }


    this.verifyOtp = async({body}) => {
        const {phoneNumber,OTP} = body;
        if(!phoneNumber || !OTP) {
            return TE("Invalid Parameter: phoneNumber,OTP is required");
        }
        // check if avaible from database

        let find_query = {
            phoneNumber,OTP,latest:true,is_verified:false
        }

        if(!await alreadyExist(UserOtp,find_query)){
            return TE("Invalid Otp");
        }
        // check if user is already exists.
        const isExist = await alreadyExist(User,{phoneNumber});
        if(!isExist){
            // create new User Instance
            await User.create({
                phoneNumber
            })
        }

        await UserOtp.findOneAndUpdate(
            { phoneNumber,OTP },
            { 
                latest:false,
                is_verified:true
            }
        );

        const loginUser = await User.findOne({phoneNumber});
        const token = jwt.sign({ _id: loginUser._id }, process.env.LOGIN_SECRETE, { expiresIn: '30 days' });
        const users = await User.findById(loginUser._id);
        return {
            data:{
                accessToken: token,
                message: "Login Successfull",
                user: users
            }
        };
    }

    this.updateProfile = async ({body,decoded}) => {
        const user_id = decoded._id;
        const data = {};
        Object.keys(body).map(item => {
            if(body[item] != "" && body[item] != undefined){
                data[item] = body[item];
            }
        })
        const updateUser = await makeInstance(User,data,user_id);
        return {
            data:{
                updateUser,
                message:"Profile Updated"
            }
        }
    } 



    this.all_address = async (user_id) => {
        return await Address.find({user_id}).sort({'updatedAt':1})
    }
    this.addAddress = async({body,decoded}) => {
        const {
            name,address,landmark,state,pincode,mobile_number
        } = body;
        const user_id = decoded._id;
        if(!name || !address || !landmark || !state || !pincode || !mobile_number) {
            return TE('Invalid Parameter: name,address,landmark,state,pincode,mobile_number is required');
        }
        // check if name is already exists
        if(await alreadyExist(Address,{user_id,name})){
            return TE("Address Name is already exists.");
        }

        const data = {name,address,landmark,state,pincode,mobile_number,user_id};

        const newAddress = await makeInstance(Address,data);
        const all_user_address = await all_address(user_id);
        return {
            data:{
                newAddress,
                all_user_address,
                total:all_user_address.length
            }
        }
    }
    return this;
  })();