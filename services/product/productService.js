const Category = require('../../models/category')
const Product = require('../../models/product');
const SubCategory = require('../../models/subCategory')
const City = require('../../models/city')
const HeathCondition = require('../../models/heath_condition');
const {current_cart_items} = require("../../services/cart/cartService")

module.exports = (function () {
    this.create = async({body}) => {
        const {name,coverImage,imageArray,pharmaCompanyId,about,
                composition,quantity,max_retail_price,selling_price,
                expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
                weight,width,length,status} = body;
        if(!composition || !cityId || !expiry_date || !imageArray ||  !quantity || !max_retail_price || !selling_price || !name || !pharmaCompanyId || !categoryId || !subCategoryId || !heathConditionId || !weight || !width || !length || !coverImage){
            return TE("Invalid Parameter: name,imageArray,pharmaCompanyId,about,composition,quantity,max_retail_price,selling_price,expiry_date,cityId,categoryId,subCategoryId,heathConditionId,weight,width,length,status,coverImage is required");
        }
      
        // check if title Exists
        if(!await alreadyExist(Category,{_id:categoryId})){
            return TE("Category Does not Already Exists");
        }
        // check if Subtitle Exists
        if(!await alreadyExist(SubCategory,{category_id:categoryId,_id:subCategoryId})){
            return TE("Sub Category Does not Already Exists");
        }
        // cityId
        if(!await alreadyExist(City,{_id:cityId})){
            return TE("City Does not Already Exists");
        }

        // check if title Exists
        if(!await alreadyExist(HeathCondition,{_id:heathConditionId})){
            return TE("Heath Condition Does not Already Exists");
        }

        const data = {name,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status,coverImage,discount_amount:max_retail_price-selling_price}
        const instance = await makeInstance(Product,data,null);
        return {
            data: {
                message:"New Product Added",
                ProductInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {name,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status,product_id,coverImage} = body;
        if(!composition || !cityId || !expiry_date || !imageArray ||  !quantity || !max_retail_price || !selling_price || !name || !pharmaCompanyId || !categoryId || !subCategoryId || !heathConditionId || !weight || !width || !length || !product_id || !coverImage){
            return TE("Invalid Parameter: name,coverImage,imageArray,pharmaCompanyId,about,composition,quantity,max_retail_price,selling_price,expiry_date,cityId,categoryId,subCategoryId,heathConditionId,weight,width,length,status is required");
        }
    
        // check if product Exists
        if(!await alreadyExist(Product,{_id:product_id})){
            return TE("Category Does not Already Exists");
        }
        // check if title Exists
        if(!await alreadyExist(Category,{_id:categoryId})){
            return TE("Category Does not Already Exists");
        }
        // check if Subtitle Exists
        if(!await alreadyExist(SubCategory,{category_id:categoryId,_id:subCategoryId})){
            return TE("Sub Category Does not Already Exists");
        }
        // cityId
        if(!await alreadyExist(City,{_id:cityId})){
            return TE("City Does not Already Exists");
        }

        // check if title Exists
        if(!await alreadyExist(HeathCondition,{_id:heathConditionId})){
            return TE("Heath Condition Does not Already Exists");
        }

        const data = {name,coverImage,imageArray,pharmaCompanyId,about,
            composition,quantity,max_retail_price,selling_price,
            expiry_date,cityId,categoryId,subCategoryId,heathConditionId,
            weight,width,length,status,discount_amount:max_retail_price-selling_price}
        const instance = await makeInstance(Product,data,product_id);
        return {
            data: {
                message:"New Product Updated",
                ProductInstance:instance
            }
        }
    }
    this.getAllView = async({body,decoded}) => {
        const {name,categoryId,subCategoryId,heathConditionId,heathConditionName,skip,limit,pharmaCompanyId} = body;
        const searchParams = {};
        const user_id = decoded._id;
        searchParams['status'] = true;
        if(pharmaCompanyId != "" && pharmaCompanyId != undefined){
            searchParams['pharmaCompanyId'] = pharmaCompanyId
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        if(categoryId != "" && categoryId != undefined){
            searchParams['categoryId'] = { "$in": categoryId }
        }
        if(subCategoryId != "" && subCategoryId != undefined){
            searchParams['subCategoryId'] = { "$in": subCategoryId }
        }
        if(heathConditionId != "" && heathConditionId != undefined){
            searchParams['heathConditionId'] = { "$in": heathConditionId }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        const all_data = await Product.find(searchParams).populate('coverImage').populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'}).sort({'s_no':1}).skip(skip).limit(limit).lean()
        const cart_items = await current_cart_items(user_id);   
        const all_products_ids =  cart_items.map(item => item.product_id._id.toString())
        all_data.map((value,key) => {
            if(all_products_ids.includes(value._id.toString())){
                value.in_cart = true
            }else{
                value.in_cart = false
            }
        })

        return {
            data:{
                message:"All Product",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    this.getDiscountedProductView = async({body,decoded}) => {
        const {status,name,categoryId,subCategoryId,heathConditionId,heathConditionName,skip,limit,pharmaCompanyId} = body;
        const user_id = decoded._id;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(pharmaCompanyId != "" && pharmaCompanyId != undefined){
            searchParams['pharmaCompanyId'] = pharmaCompanyId
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        if(categoryId != "" && categoryId != undefined){
            searchParams['categoryId'] = { "$in": categoryId }
        }
        if(subCategoryId != "" && subCategoryId != undefined){
            searchParams['subCategoryId'] = { "$in": subCategoryId }
        }
        if(heathConditionId != "" && heathConditionId != undefined){
            searchParams['heathConditionId'] = { "$in": heathConditionId }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        searchParams['discount_amount'] = { "$gt": 1 };
        const all_data = await Product.find(searchParams).populate('coverImage').populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'}).sort({'discount_amount':-1}).skip(skip).limit(limit).lean()
        
        const cart_items = await current_cart_items(user_id);   
        const all_products_ids =  cart_items.map(item => item.product_id._id.toString())
        all_data.map((value,key) => {
            if(all_products_ids.includes(value._id.toString())){
                value.in_cart = true
            }else{
                value.in_cart = false
            }
        })
        return {
            data:{
                message:"All Product",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,name,categoryId,subCategoryId,heathConditionId,heathConditionName,skip,limit,pharmaCompanyId} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(pharmaCompanyId != "" && pharmaCompanyId != undefined){
            searchParams['pharmaCompanyId'] = pharmaCompanyId
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        if(categoryId != "" && categoryId != undefined){
            searchParams['categoryId'] = { "$in": categoryId }
        }
        if(subCategoryId != "" && subCategoryId != undefined){
            searchParams['subCategoryId'] = { "$in": subCategoryId }
        }
        if(heathConditionId != "" && heathConditionId != undefined){
            searchParams['heathConditionId'] = { "$in": heathConditionId }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        if(heathConditionName != "" && heathConditionName != undefined){
            const heathCondition = await HeathCondition.find({name:new RegExp(heathConditionName,'i')}).select('id').lean()
            const heathConditionIds = heathCondition.map(record => record._id);
            searchParams['heathConditionId'] = { "$in": heathConditionIds }
        }
        const all_data = await Product.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Product",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const product = await Product.findById(id)
        if(product == null) {
            return TE('Cannot Find Product');
        }
        await product.delete();
        return {
            data:{
                message:"Product Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {product_id,status} = body
        if(!product_id && status != undefined){
            return TE("Invalid Parameter: product_id,status is required");
        }
        // check if product Exists
        if(!await alreadyExist(Product,{_id:product_id})){
            return TE("Product Does not Already Exists");
        }
        const data = {status}
        const instance = await makeInstance(Product,data,product_id);
        return {
            data: {
                message:"Product Status Updated",
                ProductInstance:instance
            }
        }
    }
    this.getIndividualProduct = async({params}) => {
        const {id} = params;
        // check if product Exists
        if(!await alreadyExist(Product,{_id:id})){
            return TE("Product Does not Already Exists");
        }
        const product = await Product.findById(id).populate('coverImage').populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'})
        return {
            data:{
                message:"Individual Product",
                product
            }
        }
    }
    return this;
  })();