const Category = require('../../models/category')
const Coupon = require('../../models/coupon')
const Banner = require('../../models/banner');
const SubCategory = require('../../models/subCategory')
const City = require('../../models/city')
const HeathCondition = require('../../models/heath_condition');
const Product = require('../../models/product');

module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,image_id,status,url} = body;
        if(!name || !image_id || !s_no){
            return TE("Invalid Parameter: name,image_id,status is required");
        }

        // check if name Exists
        if(await alreadyExist(Banner,{s_no:s_no})){
            return TE("Banner S No is Already Exists");
        }
        
        // check if name Exists
        if(await alreadyExist(Banner,{name:name})){
            return TE("Banner Name is Already Exists");
        }

        const data = {s_no,name,image_id,status,url}
        const instance = await makeInstance(Banner,data,null);
        return {
            data: {
                message:"New Banner Added",
                BannerInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {s_no,name,image_id,status,banner_id,url} = body;
        if(!name || !image_id || !s_no || !banner_id){
            return TE("Invalid Parameter: name,image_id,status,banner_id is required");
        }
        const data = {s_no,name,image_id,status,url}
        const instance = await makeInstance(Banner,data,banner_id);
        return {
            data: {
                message:"Banner Updated",
                BannerInstance:instance
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,skip,limit} = body;
        const searchParams = {};
        if(status == true || status == false) {
            searchParams['status'] = status;
        }
        console.log(searchParams);
        const all_data = await Banner.find(searchParams).populate('image_id').sort({s_no:1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Banner",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const banner = await Banner.findById(id)
        if(banner == null) {
            return TE('Cannot Find Banner');
        }
        await banner.delete();
        return {
            data:{
                message:"Banner Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {banner_id,status} = body
        if(!banner_id && status != undefined){
            return TE("Invalid Parameter: banner_id,status is required");
        }
        // check if product Exists
        if(!await alreadyExist(Banner,{_id:banner_id})){
            return TE("Banner Does not Already Exists");
        }
        const data = {status}
        const instance = await makeInstance(Banner,data,banner_id);
        return {
            data: {
                message:"Banner Status Updated",
                BannerInstance:instance
            }
        }
    }
    this.getBannerView = async() => {
        const all_data = await Banner.find({status:1}).populate('image_id').select('-status').sort({s_no:1});
        return {
            data:{
                message:"All Banner",
                all_data,
                total_count:all_data.length,
            }
        }
    }
    return this;
  })();