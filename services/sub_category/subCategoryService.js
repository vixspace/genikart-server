const Category = require('../../models/category')
const SubCategory = require('../../models/subCategory')




module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,logo,category_id,status} = body;
        const _id = category_id;
        if(!s_no || !name || !category_id){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        // check for Category Exists
        if(await alreadyExistTitle(Category,_id)){
            return TE("Cannot Find the Category");
        }
        if(await alreadyExist(SubCategory,{s_no:s_no})){
            return TE("Sub Category S_No Already Exists");
        }
        const data = {s_no,name,logo,status,category_id}
        const instance = await makeInstance(SubCategory,data,null);
        return {
            data: {
                message:"New Sub Category Added",
                subCategoryInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {s_no,name,logo,status,category_id,sub_category_id} = body;
        if(!s_no || !name || !logo || !category_id || !sub_category_id){
            return TE("Invalid Parameter: s_no,name,logo,category_id,sub_category_id is required");
        }
        const data = {s_no,name,logo,status,category_id}
        const instance = await makeInstance(SubCategory,data,sub_category_id);
        return {
            data: {
                message:"Sub Category Updated",
                subCategoryCompanyInstance:instance
            }
        }
    }
    this.getAllView = async({body}) => {
        const {status,name,skip,limit,category_id} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
        }
        if(category_id != "" && category_id != undefined){
            searchParams['category_id'] = category_id;
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        searchParams['status'] = true;
        const all_data = await SubCategory.find(searchParams).populate('logo').populate('category_id').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Sub Category",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,name,skip,limit,category_id} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(category_id != "" && category_id != undefined){
            searchParams['category_id'] = category_id;
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        const all_data = await SubCategory.find(searchParams).populate('logo').populate('category_id').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Sub Category",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const sub_category = await SubCategory.findById(id)
        if(sub_category == null) {
            return TE('Cannot Find Sub Category');
        }
        await sub_category.delete();
        return {
            data:{
                message:"Sub Category Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {sub_category_id,status} = body
        if(!sub_category_id && status != undefined){
            return TE("Invalid Parameter: sub_category_id,status is required");
        }
        const data = {status}
        const instance = await makeInstance(SubCategory,data,sub_category_id);
        return {
            data: {
                message:"Category Status Updated",
                categoryInstance:instance
            }
        }
    }
    return this;
  })();