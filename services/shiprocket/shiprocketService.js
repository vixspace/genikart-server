const axios = require('axios');
module.exports = (function () {
   
    this.getShipRocketToken = async() => {
        var data = JSON.stringify({
            "email": process.env.SHIPROCKET_EMAIL,
            "password": process.env.SHIPROCKET_PASSWORD
          });
          
          var config = {
            method: 'post',
            url: 'https://apiv2.shiprocket.in/v1/external/auth/login',
            headers: { 
              'Content-Type': 'application/json'
            },
            data : data
          };
          const response = await axios(config)
          const token = response.data.token
          return token;
    }
    this.shipOrder = async(order,length,breadth,height,weight) => {
      const address = order.address_id
      var products = []
      order.products.map(item => {
        products.push({
          "name": item.product.name,
          "sku": item.product._id,
          "units": item.quantity,
          "selling_price": item.product.selling_price,
          "discount": "",
          "tax": "",
          "hsn": ""
        })
      })
      const user = order.user_id
      var data = JSON.stringify({
        "order_id": "GENIKART-"+order._id,
        "order_date": new Date,
        "pickup_location": process.env.SHIPROCKET_PICKUP_LOCATION_ID,
        "channel_id": "",
        "comment": "",
        "billing_customer_name": user.first_name,
        "billing_last_name": user.last_name,
        "billing_address": address.address,
        "billing_address_2": address.landmark,
        "billing_city": address.city,
        "billing_pincode": address.pincode,
        "billing_state": address.state,
        "billing_country": "India",
        "billing_email": address.email,
        "billing_phone": address.mobile_number,
        "shipping_is_billing": true,
        "shipping_customer_name": "",
        "shipping_last_name": "",
        "shipping_address": "",
        "shipping_address_2": "",
        "shipping_city": "",
        "shipping_pincode": "",
        "shipping_country": "",
        "shipping_state": "",
        "shipping_email": "",
        "shipping_phone": "",
        "order_items": products,
        "payment_method": order.type == 1 ? 'Prepaid' : 'COD',
        "shipping_charges": 0,
        "giftwrap_charges": 0,
        "transaction_charges": 0,
        "total_discount": 0,
        "sub_total": order.amount.grandAmount,
        "length": length,
        "breadth": breadth,
        "height": height,
        "weight": weight
      });

      var token = await this.getShipRocketToken();
      var config = {
        method: 'post',
        url: 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
        headers: { 
          'Content-Type': 'application/json', 
          'Authorization': 'Bearer '+token
        },
        data : data
      };
      
      const response = await axios(config).catch(function (error) {
        console.log(error.response.data);
      });
      console.log(response.data);
      if(response.data.order_id){
        return response.data
      }else{
        return TE("Error Occured: " + response.data);
      }
    }
    this.shippingInformation = async(order_id,shipment_id) => {
      var token = await this.getShipRocketToken();
      const awb_number = "1504824406302";
      var config = {
        method: 'get',
        url: 'https://apiv2.shiprocket.in/v1/external/courier/track/shipment/'+shipment_id,
        headers: { 
          'Content-Type': 'application/json', 
          'Authorization': 'Bearer '+token
        }
      };
      const response = await axios(config);
      console.log(response);
      // return response.data
      return {
        "tracking_data": {
          "track_status": 1,
          "shipment_status": 3,
          "shipment_track": [
            {
              "id": 8087109,
              "awb_code": "788830567028",
              "courier_company_id": 2,
              "shipment_id": null,
              "order_id": 16255275,
              "pickup_date": null,
              "delivered_date": null,
              "weight": "2.5",
              "packages": 1,
              "current_status": "Pickup Generated",
              "delivered_to": "New Delhi",
              "destination": "New Delhi",
              "consignee_name": "Naruto",
              "origin": "Jammu",
              "courier_agent_details": null
            }
          ],
          "shipment_track_activities": [
            {
              "date": "2019-08-01 05:20:55",
              "activity": "Shipment information sent to FedEx - OC",
              "location": "NA"
            }
          ],
          "track_url": "https://app.shiprocket.in/tracking/awb/788830567028"
        }
      }
    }
    return this;
  })();