const Product = require('../../models/product');
const Cart = require('../../models/cart');

module.exports = (function () {
    
    this.makeCartByOrder = async(user_id,order_id) => {
        await Cart.update({user_id,in_cart:true},{
            order_id,in_cart:false
        },{upsert: true})
    }
    this.current_cart_items = async (user_id) => {
        const products_all = {
            path:'product_id',
            populate: [{
                path: 'cityId',
                model: 'City'
            },
            {
                path: 'categoryId',
                model: 'Category'
            },
            {
                path: 'subCategoryId',
                model: 'SubCategory'
            },
            {
                path: 'heathConditionId',
                model: 'HeathCondition'
            },
            {
                path: 'imageArray',
                model: 'Image'
            },
            {
                path: 'coverImage',
                model: 'Image'
            },
            {
                path: 'pharmaCompanyId',
                model: 'PharmaCompany'
            },
            ]
        }
        return await Cart.find({
            user_id,in_cart:true
        }).populate(products_all).sort({'updatedAt':-1})
    }
    this.addUpdateToCart = async({body,decoded}) => {
        const {
            product_id,quantity 
        } = body
        const user_id = decoded._id;
        if(!product_id || !quantity || !user_id){
            return TE('Invalid Parameter: product_id, quantity is required');
        }
        // check if product is avaible
        if(!await alreadyExist(Product,{status:true,_id:product_id})){
            return TE('Product is not avaible');
        }
        const product = await Product.findById(product_id).select('quantity')
        if(product.quantity < quantity){
            return TE('Product Quantity not avaible');
        }
        const cart = await Cart.findOneAndUpdate({product_id,user_id,in_cart:true},{$set:{quantity:quantity}},{new: true,upsert: true,});
        const all_cart_items = await current_cart_items(user_id)
        return {
            data:{
                updatedItem:cart,
                all_cart_items,
                total_item:all_cart_items.length,
                message:"Cart Updated",
            }
        }
    }


    this.getCartItem = async({decoded}) => {
        const user_id = decoded._id;
        const all_cart_items = await current_cart_items(user_id)
        return {
            data:{
                all_cart_items,
                total_item:all_cart_items.length,
                message:"All Cart Product"
            }
        }
    }
        
    return this;
  })();