const City = require('../../models/city')
const CitySetting = require('../../models/citySetting')
const PharmaCompany = require('../../models/PharmaCompany')
const RazorPayLog = require('../../models/razorpayLog')
const Wallet = require('../../models/walletLog')
const User = require('../../models/user')
const Notification = require('../../models/notification')
const crypto = require('crypto')

const secret_key = '12345678'
const Razorpay = require('razorpay')
const razorpay = new Razorpay({
    key_id: process.env.RAZORPAY_KEY_ID,
    key_secret: process.env.RAZORPAY_KEY_SECRET
})



module.exports = (function () {
    /**
     * @params body
     * @For callback from razorpay
     */
    this.callback = async({body}) => {
        const {event,payload} = body

        if(event == "payment.authorized"){
            const {entity} = payload.payment
            const {id,amount,status,notes} = entity
            const order_id = notes.order_id;
            const razorpayLogInstance = await RazorPayLog.findOne({order_id}) 
            // if(razorpayLogInstance == null || razorpayLogInstance.status != 0){
            //     return {
            //         "message":"Already Transactioned"
            //     }
            // }
            razorpayLogInstance.razorpay_id = id;
            razorpayLogInstance.status = 1;

            const newWalletInstance = await Wallet.create({
                user_id:razorpayLogInstance.user_id,
                credit:razorpayLogInstance.amount,
                description:"Transaction With Online",
                razorpay_id:id,
                transaction_id:generateRandomNumber(6),
                status:true,
            })

            razorpayLogInstance.razorpay_id = id;
            razorpayLogInstance.wallet_id = newWalletInstance._id;
            razorpayLogInstance.callbackJson = body;
            console.log(await razorpay.payments.capture(id, amount, "INR"))
            const user = await User.findById(razorpayLogInstance.user_id);
            if(user.fcmToken != ""){
                const message_notification = {
                    notification: {
                      title: "Money Added to Wallet",
                      body: razorpayLogInstance.amount+" has been credit to wallet.",
                    },
                };
                await Notification.create({
                    user_id:user._id,
                    title:message_notification.notification.title,
                    body:message_notification.notification.body
                })
                await sendNotification(user.fcmToken,message_notification)
            }
            await razorpayLogInstance.save();
        }
    }
    return this;
  })();