const Category = require('../../models/category')
const Coupon = require('../../models/coupon')
const SubCategory = require('../../models/subCategory')
const City = require('../../models/city')
const HeathCondition = require('../../models/heath_condition');
const Product = require('../../models/product');

module.exports = (function () {
    this.create = async({body}) => {
        const {code,description,terms_condition,start_date_time,
            end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,
            max_discount_amount,status} = body;
        if(!code || !start_date_time || !end_date_time || !discount_percentage ||  !minimum_billing_amount || !max_user_applicable_orders || !max_discount_amount ){
            return TE("Invalid Parameter: code,start_date_time,end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,max_discount_amount,status is required");
        }

        // check if name Exists
        if(await alreadyExist(Coupon,{code:code})){
            return TE("Coupon Code is Already Exists");
        }

        const data = {code,description,terms_condition,start_date_time,
            end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,
            max_discount_amount,status}
        const instance = await makeInstance(Coupon,data,null);
        return {
            data: {
                message:"New Coupon Added",
                CouponInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {coupon_id,code,description,terms_condition,start_date_time,
            end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,
            max_discount_amount,status} = body;
            if(!code || !start_date_time || !end_date_time || !discount_percentage ||  !minimum_billing_amount || !max_user_applicable_orders || !max_discount_amount ){
                return TE("Invalid Parameter: code,start_date_time,end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,max_discount_amount,status is required");
            }

        const data = {code,description,terms_condition,start_date_time,
            end_date_time,discount_percentage,minimum_billing_amount,max_user_applicable_orders,
            max_discount_amount,status}
        const instance = await makeInstance(Coupon,data,coupon_id);
        return {
            data: {
                message:"Coupon Updated",
                CouponInstance:instance
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,skip,code,limit} = body;
        const searchParams = {};
        if(status == true || status == false) {
            searchParams['status'] = status;
        }
        if(code != ""  && code != undefined){
            searchParams['code'] = new RegExp(code,'i');
        }
        console.log(searchParams)

        const all_data = await Coupon.find(searchParams).populate('image_id').skip(skip).limit(limit)
        return {
            data:{
                message:"All Coupon",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const coupon = await Coupon.findById(id)
        if(coupon == null) {
            return TE('Cannot Find Coupon');
        }
        await coupon.delete();
        return {
            data:{
                message:"Coupon Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {coupon_id,status} = body
        if(!coupon_id && status != undefined){
            return TE("Invalid Parameter: coupon_id,status is required");
        }
        // check if product Exists
        if(!await alreadyExist(Coupon,{_id:coupon_id})){
            return TE("Coupon Does not Already Exists");
        }
        const data = {status}
        const instance = await makeInstance(Coupon,data,coupon_id);
        return {
            data: {
                message:"Coupon Status Updated",
                CouponInstance:instance
            }
        }
    }
    this.getAllView = async({body}) => {
        const {name,code,skip,limit} = body;
        const searchParam = {};
        console.log(body)
        if(code != ""){
            searchParam['code'] = new RegExp(code,'i');
        }
        // searchParam['status'] = true;
        console.log(searchParam)
        const all_data = await Coupon.find(searchParam).skip(skip).limit(limit);
        return {
            data:{
                all_data,
                message:"All Coupon"
            }
        }
    }
    return this;
  })();