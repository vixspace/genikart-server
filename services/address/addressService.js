const Category = require('../../models/category')
const User = require('../../models/user')
const Address = require('../../models/address')
const UserOtp = require('../../models/userOtp')
const jwt = require("jsonwebtoken");
const { plugin } = require('mongoose');



module.exports = (function () {

    this.all_address = async (user_id,skip = null,limit = null ) => {
        return await Address.find({user_id,status:true}).sort({'updatedAt':-1}).select('-status').skip(skip).limit(limit)
    }
    this.create = async({body,decoded}) => {
        const {
            name,address,landmark,state,pincode,mobile_number,city
        } = body;
        const user_id = decoded._id;
        if(!name || !address || !landmark || !state || !pincode || !mobile_number) {
            return TE('Invalid Parameter: name,address,landmark,state,pincode,mobile_number is required');
        }
        // check if name is already exists
        if(await alreadyExist(Address,{user_id,name,status:true})){
            return TE("Address Name is already exists.");
        }

        const data = {name,address,landmark,state,pincode,mobile_number,city,user_id};

        const newAddress = await makeInstance(Address,data);
        const all_user_address = await all_address(user_id);
        return {
            data:{
                newAddress,
                all_user_address,
                total:all_user_address.length,
                message:"New Address Added"
            }
        }
    }
    this.getAll = async({body,decoded}) => {
        const user_id = decoded._id;
        const {skip,limit} = body;
        const all_user_address = await all_address(user_id,skip,limit);
        return {
            data:{
                all_user_address,
                total:all_user_address.length,
                message:"All Address"
            }
        }
    }
    this.edit = async({body,decoded}) => {
        const {
            name,address,landmark,state,pincode,mobile_number,address_id,city
        } = body;
        const user_id = decoded._id;
        if(!name || !address || !landmark || !state || !pincode || !mobile_number || !address_id) {
            return TE('Invalid Parameter: name,address,landmark,state,pincode,mobile_number,address_id is required');
        }
        const data = {name,address,landmark,state,pincode,mobile_number,city,user_id};
        const updatedAddress = await makeInstance(Address,data,address_id);
        const all_user_address = await all_address(user_id);
        return {
            data:{
                updatedAddress,
                all_user_address,
                total:all_user_address.length,
                message:"Address Updated"
            }
        }
    }
    this.remove = async({params,decoded}) => {
       const user_id = decoded._id;
       const {id} = params
        // check if name is already exists
        if(!await alreadyExist(Address,{user_id,_id:id,status:true})){
            return TE("Cannot Found Address");
        }    
        await Address.findOneAndUpdate({_id:id},{$set:{status:false}});
        const all_user_address = await all_address(user_id);
        return {
            data:{
                all_user_address,
                total:all_user_address.length,
                message:"Address Removed"
            }
        }   
    }
    return this;
  })();