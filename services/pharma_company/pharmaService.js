const City = require('../../models/city')
const CitySetting = require('../../models/citySetting')
const PharmaCompany = require('../../models/PharmaCompany')



module.exports = (function () {
    /**
     * @params body
     * @For create new City
     */
    this.makePharmaCompanyInstance = async(data,id) => {
        let instance;
        if(id == null){
            instance = new PharmaCompany;
        }else{
            instance = await PharmaCompany.findById(id);
            if(instance == null){
                return TE('Cannot Found PharmaCompany')
            }
        }
        Object.keys(data).map((key,) => {
            instance[key] = data[key];
        })
        return await instance.save();
    }

    this.createPharma = async({body}) => {
        const {s_no,name,logo,status} = body;
        if(!s_no || !name ){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        const pharmaData = {s_no,name,logo,status}
        const pharmaCompanyInstance = await makePharmaCompanyInstance(pharmaData,null);
        return {
            data: {
                message:"New Pharma Company Added",
                pharmaCompanyInstance
            }
        }
    }

    this.updatePharma = async({body}) => {
        const {s_no,name,logo,status,pharma_id} = body;
        if(!s_no || !name || !logo || !pharma_id){
            return TE("Invalid Parameter: s_no,name,logo,pharma_id is required");
        }
        const data = {s_no,name,logo,status}
        const pharmaCompanyInstance = await makePharmaCompanyInstance(data,pharma_id);
        return {
            data: {
                message:"Pharma Company Updated",
                pharmaCompanyInstance
            }
        }
    }
    this.getAllView = async({body}) => {
        const {status,skip,limit} = body;
        const searchParams = {};
        searchParams['status'] = true;
        const all_data = await PharmaCompany.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Pharma Companys",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    this.getAllPharma = async({body}) => {
        const {status,skip,limit} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        const all_data = await PharmaCompany.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Pharma Companys",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.deletePharma = async({params}) => {
        const {pharma_id} = params
        const pharma = await PharmaCompany.findById(pharma_id)
        if(pharma == null) {
            return TE('Cannot Find Pharma Company');
        }
        await pharma.delete();
        return {
            data:{
                message:"Pharma Company Deleted",
            }
        }
    }

    this.updatePharmaStatus = async({body}) => {
        const {pharma_id,status} = body
        if(!pharma_id && status != undefined){
            return TE("Invalid Parameter: pharma_id,status is required");
        }
        const data = {status}
        const pharmaInstance = await makePharmaCompanyInstance(data,pharma_id);
        return {
            data: {
                message:"Pharma Company Status Updated",
                pharmaInstance
            }
        }
    }
    return this;
  })();