const Category = require('../../models/category')
const User = require('../../models/user')
const Product = require('../../models/product')
const HeathCondition = require('../../models/heath_condition')
const PharmaCompany = require('../../models/PharmaCompany')
const WalletLog = require('../../models/walletLog')
const City = require('../../models/city')
const Notification = require('../../models/notification')



const Address = require('../../models/address')
const UserOtp = require('../../models/userOtp')
const RazorpayLog = require('../../models/razorpayLog')
const jwt = require("jsonwebtoken");
const { ObjectId } = require('mongodb');
var Razorpay = require('razorpay')
const FlipkartProduct = require('../../models/flipkartProduct')




module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,logo,status} = body;
        if(!s_no || !name ){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        // check if title Exists
        if(await alreadyExistTitle(Category,name)){
            return TE("Category Already Exists");
        }

        if(await alreadyExist(Category,{s_no:s_no})){
            return TE("City S_No Already Exists");
        }

        const data = {s_no,name,logo,status}
        const instance = await makeInstance(Category,data,null);
        return {
            data: {
                message:"New Category Added",
                categoryInstance:instance
            }
        }
    }

    this.sendOtp = async({body}) => {
        const {phoneNumber} = body;
        if(!phoneNumber && !validatePhone(phoneNumber)){
            return TE("Invalid Parameter: phonenumbe is required");
        }
        const OTP = generateVerificationCode();

        let instance_data = {
            phoneNumber,
            OTP
        }
        const user_otp_instance = await makeInstance(UserOtp,instance_data);
        if(user_otp_instance == null){
            return TE("Issue in generating OTP. Please try again later.");
        }
        await sendLoginOtp(user_otp_instance);
        return {
            data:{
                message:"Otp Sended successfully"
            }
        }
    }


    this.verifyOtp = async({body}) => {
        const {phoneNumber,OTP} = body;
        if(!phoneNumber || !OTP) {
            return TE("Invalid Parameter: phoneNumber,OTP is required");
        }
        // check if avaible from database

        let find_query = {
            phoneNumber,OTP,latest:true,is_verified:false
        }

        if(!await alreadyExist(UserOtp,find_query)){
            return TE("Invalid Otp");
        }
        // check if user is already exists.
        const isExist = await alreadyExist(User,{phoneNumber});
        if(!isExist){
            // create new User Instance
            await User.create({
                phoneNumber
            })
        }

        await UserOtp.findOneAndUpdate(
            { phoneNumber,OTP },
            { 
                latest:false,
                is_verified:true
            }
        );

        const loginUser = await User.findOne({phoneNumber});
        const token = jwt.sign({ _id: loginUser._id }, process.env.LOGIN_SECRETE, { expiresIn: '30 days' });
        const users = await User.findById(loginUser._id);
        return {
            data:{
                accessToken: token,
                message: "Login Successfull",
                user: users
            }
        };
    }

    this.updateProfile = async ({body,decoded}) => {
        const user_id = decoded._id;
        const data = {};
        Object.keys(body).map(item => {
            if(body[item] != "" && body[item] != undefined){
                data[item] = body[item];
            }
        })
        const updateUser = await makeInstance(User,data,user_id);
        const user = await getUserInstance(user_id);
        return {
            data:{
                user,
                message:"Profile Updated"
            }
        }
    } 

    this.submitForApproval = async ({body,decoded}) => {
        const user_id = decoded._id;
        if(!await alreadyExist(User,{_id:user_id})){
            return TE('Cannot Find User');
        }
        const updatedUser = await User.findByIdAndUpdate(user_id,{status:1},{new: true},);
        const user = await getUserInstance(user_id);
        return {
            data:{
                user,
                message:"User sent for KYC"
            }
        }
    }
    this.getUserInstance = async (user_id) => {
        return await User.findById(user_id).populate('profilePic').populate('gst_image').populate('drug_image')
    }
    this.getUserInformation = async({body,decoded}) => {
        const user_id = decoded._id;
        if(!await alreadyExist(User,{_id:user_id})){
            return TE("Cannot Found User");
        }
        const current_wallet_amount = await walletFinalAmount(user_id);
        const user = await getUserInstance(user_id);
        return {
            data:{
                user,
                current_wallet_amount,
                message:"All User Information"
            }
        }
    }
    this.globalSearch = async({body}) => {
        const {search,skip,limit} = body;
        const searchParams = {};
        searchParams['name'] = new RegExp(search,'i');

        const pharma_company = await PharmaCompany.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        const heath_condition = await HeathCondition.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        const category = await Category.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        const products = await Product.find(searchParams).populate('coverImage').populate('imageArray').populate({path:'pharmaCompanyId',populate:'logo'}).populate({path:'categoryId',populate:'logo'}).populate({path:'subCategoryId',populate:'logo'}).populate({path:'heathConditionId',populate:'logo'}).sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"Global Search",
                result:{
                    pharma_company,
                    heath_condition,
                    category,
                    products
                }
            }
        }
    }
    
    this.addAmounToWallet = async({body}) => {
        const {user_id,amount} = body;
        const newWalletLog = await WalletLog.create({
            credit:amount,debit:0,description:"Development Only",user_id,transaction_id:generateRandomNumber(6)
        })
        const current_wallet_amount = await walletFinalAmount(user_id);
        return {
            data:{
                newWalletLog,
                current_wallet_amount,
            }
        }
    }
    this.addAmountRefund = async(user_id,order_id,amount) => {
        const newWalletLog = await WalletLog.create({
            credit:amount,debit:0,description:"Cancel Order Refund",user_id,order_id,transaction_id:generateRandomNumber(6)
        })
        const current_wallet_amount = await walletFinalAmount(user_id);
        return {
            data:{
                newWalletLog,
                current_wallet_amount,
            }
        }
    }
    this.formatWalletLog = (walletLog) => {
        const final_log_data = [];
        var type;
        walletLog.map(item => {
            if(item.credit > 0){
                type = 'postive'
            }else{
                type = 'negetive';
            }
            final_log_data.push({
                type,
                id:item.credit,
                transaction_id:item.transaction_id,
                amount: type == 'postive' ? item.credit : item.debit,
                description:item.description,
                createdAt:item.createdAt
            })
        });
        return final_log_data;
    }
    this.getAllWalletLog = async({body,decoded}) => {
        const {skip,limit,from,to} = body
        const user_id = decoded._id;
        const searchParams = {}
        searchParams['user_id'] = user_id;
        searchParams['status'] = true;
        if(from && to){
            searchParams['createdAt'] = {
                $gte: new Date(from),
                $lte: new Date(to)
              }
        }
        const all_wallet_data = await WalletLog.find(searchParams).sort({'createdAt':-1}).skip(skip).limit(limit).limit(limit)
        const current_wallet_amount = await walletFinalAmount(user_id);
        
        return {
            data:{
                wallet_data:formatWalletLog(all_wallet_data),
                current_wallet_amount,
                message:"Wallet Log"
            }
        }
    }
    this.orderPlaceDeductAmountWallet = async(user_id,order_id,amount) => {
        var description = "Order Placed: #".order_id;
        const newWalletLog = await WalletLog.create({
            debit:amount,credit:0,description,user_id,order_id,transaction_id:generateRandomNumber(6)
        })
        return true;
    }
    this.getAll = async({body}) => {
        const {skip,limit,name,cityId,phoneNumber} = body;
        const searchParams = {};
        if(name != "" && name != undefined){
            const user_name = await User.find( { $or:[ {'first_name':new RegExp(name,'i')}, {'last_name':new RegExp(name,'i')} ]}).distinct('_id')
            searchParams['_id'] = {"$in":user_name}
            // searchParams['first_name'] = new RegExp(name,'i');
            // searchParams['last_name'] = new RegExp(name,'i');
        }
        if(phoneNumber != "" && phoneNumber != undefined){
            searchParams['phoneNumber'] = new RegExp(phoneNumber,'i');
        }
        if(cityId != "" && cityId != undefined){
            searchParams['cityId'] = cityId;
        }
        const all_users = await User.find(searchParams).populate('cityId').populate('drug_image').populate('gst_image').sort({'updatedAt':-1}).skip(skip).limit(limit);
        return {
            data:{
                message:"All Users",
                all_users
            }
        }
    }
    this.getIndividualUser = async({params}) => {
        const {id} = params;
        const user = await User.findById(id).populate('cityId').populate('drug_image').populate('gst_image').sort({'updatedAt':-1})
        const current_wallet_amount = await walletFinalAmount(id);
        return {
            data:{
                user, 
                current_wallet_amount,
                message:"User Individual Information"
            }
        }
    }
    this.updateUserStatus = async({body}) => {
        const {user_id,status} = body;
        const user_updated = await User.findById({_id:user_id});
        user_updated.status = status;
        await user_updated.save();
        

        return {
            data:{
                user:user_updated,
                message:"User Updated"
            }
        }
    }
    this.updateFcmToken = async ({body,decoded}) => {
        const {fcmToken} = body;
        const user_id = decoded._id
        const data = {fcmToken}
        const instance = await makeInstance(User,data,user_id);
        return {
            data:{
                userUpdated:instance,
                message:"User Updated"
            }
        }
    }
    this.generateRazorpayTransaction = async ({body,decoded}) => {
        const {amount} = body
        const user_id = decoded._id;

        if(!amount){
            TE("Invalid Parameter: amount is required")
        }
        var options = {
            amount: parseInt(amount* 100),
            currency: 'INR',
            receipt: generateRandomString(),
            payment_capture: "1"
        }
        const razorpay = new Razorpay({
            key_id: process.env.RAZORPAY_KEY_ID,
            key_secret: process.env.RAZORPAY_KEY_SECRET
        })
                      
        const response = await razorpay.orders.create(options)
        const razor_pay_response = {
                        order_id: response.id,
                        currency: response.currency,
                        amount: response.amount,
                      }
        const data = {
            user_id,
            amount,
            razorpayAmount:razor_pay_response.amount,
            receipt_id:options.receipt,
            order_id:razor_pay_response.order_id
        }
        const razorpayWalletLog = await makeInstance(RazorpayLog,data,null)
        return {
            data:{
                razorpayWalletLog,
                receipt_id:options.receipt,
                order_id:razor_pay_response.order_id,
                "message":"Razorpay Amount Log"
            }
        }
        
    }
    this.updateCityIdByLatLong = async({body,decoded}) => {
        const {latitute,longitude} = body
        const user_id = decoded._id;
        const city = await City.findOne();

        const data = {cityId:city._id,latitute,longitude}
        const updatedUser = await makeInstance(User,data,user_id);
        return {
            data:{
                updatedUser,
                message:"Updated By Lat Long"
            }
        }
    }
    this.getAllNotifications = async({body,decoded}) => {
        const {skip,limit} = body
        const user_id = decoded._id;
        const searchParams = {}
        searchParams['status'] = 1
        searchParams['user_id'] = user_id;
        const notifications = await Notification.find(searchParams).skip(skip).limit(limit).sort({"updatedAt":-1})
        return {
            data:{
                allNotifications:notifications,
                totalCount:await alreadyExist(Notification,{status:1})
            }
        }
    }
    this.clearNotification = async({body,decoded}) => {
        const user_id = decoded._id;
        // const updateNotifications = await Notification.update({user_id:user_id},{$set: { status: 2 }});
        // console.log(updateNotifications)
        
        const updateNotifications = await Notification.remove({user_id:user_id})
        console.log(updateNotifications)
        return {
            data:{
                message:"Notification Has been Cleared"
            }
        }
    }

    this.flipkartProductAdd = async({body}) => {
        const {analyticsData,availability,baseUrl,id,itemId,keySpecs,listingId,images,pricing,mrp,productBrand,rating,smartUrl,titles} = body
        console.log("Product: Id -> "+id)
        const newProduct = await FlipkartProduct.create({analyticsData,availability,baseUrl,id,itemId,keySpecs,listingId,images,pricing,mrp,productBrand,rating,smartUrl,titles})
        return {
            data:{
                message:"Data Inserted Successfully"
            }
        }
    }
    return this;
  })();