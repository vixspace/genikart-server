const bcrypt = require('bcrypt')
const Admin = require('../../models/admin')
const VersionSetting = require('../../models/versionSetting')
const jwt = require("jsonwebtoken");
module.exports = (function () {
    /**
     * @params {email} admin's email
     * @params {password} admin's password
     * @For login the admin
     */
    this.loginAdmin = async({body}) => {
        // get the values
        const {email,password} = body

        // if if email is avaible
        const checkAdmin = await Admin.findOne({email:email})
        // check if credient are correct
        if (!checkAdmin || !bcrypt.compareSync(password, checkAdmin.hash)) {
            throw TE("Invalid email or password")
        }    

        const token = jwt.sign({ _id: checkAdmin._id }, process.env.LOGIN_ADMIN_SECRETE, {
            expiresIn: "3649635 days"
          });
        
        // update the superadmin with the loginToken
        await checkAdmin.updateOne({loginToken:token})

        // get the superadmin instance from the database
        const admin = await Admin.findById(checkAdmin._id)
        return {data:{admin,token,"message":"Login successfull"}}
    }


    /**
     * @params body
     * @For creating a new user
     */
     this.createAdmin = async ({ body,decoded }) => {
      /**
       * Get all the varibles name,email,password and adminId
      */
      const {name,email,password} = body;

      /** check if all varibles exists */
      if(!name || !email || !password){
          TE("Invalid Parameter: name,email,password required")
      }

      /** check if admin already exists*/
      const checkIfUserExists = await Admin.findOne({email:email})
      if(checkIfUserExists){
          TE("Admin Already Exists. Please try other email.")
      }

      // create new admin instance
      const newAdmin = new Admin({
          email: email,
          name:name,
          hash : bcrypt.hashSync(password, 10)
      })

      // save admin instance to database
      await newAdmin.save()
      return {
          data:{
              "message":"New Admin Created",
              newAdmin
          }
      }
    };

    
    /**
     * @params body
     * @For update admin password
     */
     this.updateAdminPassword = async({body,decoded}) => {
      const {password,adminId} = body
      if(!adminId){
        TE('Invalid parameters: adminId required')
      }
      if(!password){
        TE('Invalid parameters: password required')
      }
      
      // check if admin exists in database
      const updateAdmin = await Admin.findById(adminId)
      // if not exists in throw error
      if(!updateAdmin)
      {
          TE('Admin does not exist')
      }
      updateAdmin.hash = bcrypt.hashSync(password, 10)
      await updateAdmin.save()
      return {data:{"message":"Admin User Password updated",updateAdmin}}
    }

    /**
     * @params body
     * @For update the user
     */
     this.updateAdmin = async({body,decoded}) => {
      // add varibles
      const {name,password,email,adminId} = body
      if(!adminId){
          TE('Invalid parameters: adminId required')
      }

      // check if admin exists in database
      const updateAdmin = await Admin.findById(adminId)
      // if not exists in throw error
      if(!updateAdmin)
      {
          TE('Admin does not exist')
      }

      // if name exist than update name
      if(name){
          updateAdmin.name = name
      }

      //if email exist then update email
      if(email){
          updateAdmin.email = email
      }

      updateAdmin.updatedByUser = decoded._id

      // update the updateAdmin instance to database
      await updateAdmin.save()
      return {data:{"message":"Admin User updated",updateAdmin}}
    }


    /**
     * @params body
     * @For delete the user
     */
     this.deleteAdmin = async({params}) => {
      // add varibles
      const {adminId} = params
      if(!adminId){
          throw TE('Invalid parameters: adminId required')
      }

      // check if admin exists in database
      const checkAdmin = await Admin.findById(adminId)

      // if not exists in throw error
      if(!checkAdmin)
      {
          throw TE('Admin does not exist')
      }

      await checkAdmin.delete()
      return {data:{"message":"Admin User Deleted"}}
    };
    /**
     * @params body
     * @For get all admin in system
     */
     this.getAllAdmin = async({body}) => {
      const {skip,limit,name} = body
      const searchParams = {};
      if(name){
          searchParams.name = new RegExp(name, 'i')
      }
      const admins = await Admin.find(searchParams).skip(skip).limit(limit);
      return {
          data:{
            admins,
            total: admins.length,
            skip,limit
         }
       }
    }

    /**
     * @params body
     * @For get version setting for application
     */
    this.getVersionSettingInstance = async() => {
        var version = await VersionSetting.findOne()
        if(version == null){
            version = await VersionSetting.create({android_version:1,ios_version:1});
        }
        return version;
    }
    this.getVersionSetting = async() => {
        var version = await getVersionSettingInstance();
        return {
            data:{
                message:"Version Setting",
                version
            }
        }
    }

    /**
     * @params body
     * @For update version setting for application
     */
    this.updateVersionSetting = async({body}) => {
        const {android_version,ios_version} = body
        var version = await getVersionSettingInstance();
        version.android_version = android_version;
        version.ios_version = ios_version;
        version.save();
        return {
            data:{
                message:"Version Setting Updated",
                version
            }
        }
    }

    return this;
  })();