const Category = require('../../models/category')
const Coupon = require('../../models/coupon')
const Banner = require('../../models/banner');
const SubCategory = require('../../models/subCategory')
const City = require('../../models/city')
const HeathCondition = require('../../models/heath_condition');
const Product = require('../../models/product');
const Cart = require('../../models/cart');
const User = require('../../models/user');
const Order = require('../../models/order');
const Notification = require('../../models/notification')
const CitySetting = require('../../models/citySetting');
const {
    makeCartByOrder
} = require('../cart/cartService');
const {
    orderPlaceDeductAmountWallet,addAmountRefund
} = require('../user/userService');
const shiprocket = require('../shiprocket/shiprocketService');

module.exports = (function () {
    this.getProductsCartToOrder = async(user_id) => {
        const all_items = await current_cart_items(user_id);
        const products = [];
        all_items.map(item => {
            products.push({
                product:item.product_id._id,
                quantity:item.quantity
            })
        })
        return products;
    }
    this.placeOrder = async({body,decoded}) => {
        const {coupon_id, type,address_id} = body
        const user_id = decoded._id;
        if(!type || !address_id){
            return TE("Invalid Parameter: type,address_id is required");
        }
        const user = await User.findById(user_id)
        const finalAmount = await getFinalAmount(coupon_id, type,user_id)
        if(type == "wallet"){
            var current_amount = await walletFinalAmount(user_id);
            if(finalAmount.amount.grandAmount >= current_amount || current_amount <= 0){
                return TE("Wallet is less than in wallet");
            }
        }
        const orderInstance = new Order;
        orderInstance.user_id = user_id;
        orderInstance.cityId = user.cityId;
        orderInstance.coupon_id = coupon_id;
        orderInstance.type = type == "wallet" ? 2 : 1;
        orderInstance.status = 1;
        orderInstance.address_id = address_id;
        orderInstance.products = await getProductsCartToOrder(user_id);
        orderInstance.amount.cartAmount = finalAmount.amount.amount;
        orderInstance.amount.couponAmount = finalAmount.amount.couponAmount;
        orderInstance.amount.delivery_amount = finalAmount.amount.delivery_amount;
        orderInstance.amount.gateWayAmount = finalAmount.amount.gateWayAmount;
        orderInstance.amount.grandAmount = finalAmount.amount.grandAmount;
        await orderInstance.save();
        await makeCartByOrder(user_id,orderInstance._id);
        if(type == "wallet"){
            await orderPlaceDeductAmountWallet(user_id,orderInstance._id,finalAmount.amount.grandAmount);
        }
        if(user.fcmToken != ""){
            const message_notification = {
                notification: {
                  title: "Order Placed",
                  body: "New Order: has placed successfully.",
                },
              };
            await Notification.create({
                user_id:user._id,
                title:message_notification.notification.title,
                body:message_notification.notification.body
            })
            await sendNotification(user.fcmToken,message_notification)
        }
        return {
            data:{
                orderInstance,
                current_amount : await walletFinalAmount(user_id)
            },
            "message":"Order Placed"
        }
    }
    this.current_cart_items = async (user_id) => {
        const all_products = {
                path:'product_id',
                populate: [{
                    path: 'cityId',
                    model: 'City'
                },
                {
                    path: 'categoryId',
                    model: 'Category'
                },
                {
                    path: 'subCategoryId',
                    model: 'SubCategory'
                },
                {
                    path: 'heathConditionId',
                    model: 'HeathCondition'
                },
                {
                    path: 'imageArray',
                    model: 'Image'
                },
                {
                    path: 'coverImage',
                    model: 'Image'
                },
                {
                    path: 'pharmaCompanyId',
                    model: 'PharmaCompany'
                }
                ]
        };
        return await Cart.find({
            user_id,in_cart:true
        }).populate(all_products)
    }
    this.getCartAmount = async(user_id) => {
        const cartItems = await Cart.find({
            user_id,in_cart:true
        }).populate('product_id');
        let mrp_amount = discount = amount = 0;
        cartItems.map(item => {
            var {selling_price,max_retail_price,discount_amount} = item.product_id;
            var {quantity} = item
            amount += quantity * selling_price;
            mrp_amount += quantity * max_retail_price;
            discount += quantity * discount_amount;
        })
        return {
            mrp_amount,discount,amount
        }
    }
    this.calculateCouponAmount = async(coupon,final_amount) => {
        if(final_amount <= coupon.minimum_billing_amount){
            return TE("Minimum Bill Amount is "+coupon.minimum_billing_amount);
        }
        return final_amount * (coupon.discount_percentage / 100);
    }
    this.calculateGateWayAmount = async(setting,amount) => {
        return amount * (setting.payment_gateway_percentage / 100);
    }
    this.getFinalAmount = async(coupon_id, type,user_id,amount) => {
        var coupon = city = null;
        var couponAmount = finalAmount = gateWayAmount = delivery_amount = 0;
        const user = await User.findById(user_id).populate('cityId');
        if(user.cityId == null) {
            return TE('City is not set');
        }
        const setting = await CitySetting.findOne({city_id:user.cityId});
        const cartAmount = await getCartAmount(user_id)
        if(coupon_id !="" && coupon_id != undefined){
            coupon = await Coupon.findById(coupon_id)
            couponAmount = await calculateCouponAmount(coupon,cartAmount.amount)
        }
        finalAmount = cartAmount.amount - couponAmount;
        if(type == "wallet"){
            gateWayAmount = await calculateGateWayAmount(setting,finalAmount);
        }else{
            if(setting.is_cod == false){
                return TE("COD is not available for city.");
            }
        }
        if(finalAmount <= setting.free_delivery_order_price){
            delivery_amount = setting.delivery_price;
        }
        if(finalAmount <= setting.minimum_order_price){
            return TE("Minimum order price is ".setting.minimum_order_price);
        }
        grandAmount = finalAmount + delivery_amount + gateWayAmount;
        return {
            cartAmount,
            amount:{
                amount:cartAmount.amount,
                couponAmount,
                delivery_amount,
                gateWayAmount,
                grandAmount
            }
        }
    }
    this.summary = async({body,decoded}) => {
        const {coupon_id, type} = body
        const user_id = decoded._id;
        const finalAmount = await getFinalAmount(coupon_id, type,user_id)
        return {
            data:{
                finalAmount,
                current_cart_items:await current_cart_items(user_id)
            },
            message:"Order Summary"
        }
    }
    this.getAllUserOrder = async({body,decoded}) => {
        const user_id = decoded._id;

        const products_all = {
            path:'products.product',
            populate: [{
                path: 'cityId',
                model: 'City'
            },
            {
                path: 'categoryId',
                model: 'Category'
            },
            {
                path: 'subCategoryId',
                model: 'SubCategory'
            },
            {
                path: 'heathConditionId',
                model: 'HeathCondition'
            },
            {
                path: 'imageArray',
                model: 'Image'
            },
            {
                path: 'pharmaCompanyId',
                model: 'PharmaCompany'
            },
            {
                path: 'coverImage',
                model: 'Image'
            },
            {
                path: 'address_id',
                model: 'Address'
            }
            ]
        }

        const all_orders = await Order.find({user_id}).populate('address_id').populate(products_all).populate('coupon_id').sort({'updatedAt':-1});
        return {
            data:{
                message:"All Order",
                all_orders
            }
        }
    }
    this.getAll = async({body}) => {
        const {skip,limit,type,from,to,cityId,phoneNumber,name} = body;
        const products_all = {
            path:'products.product',
            populate: [{
                path: 'cityId',
                model: 'City'
            },
            {
                path: 'categoryId',
                model: 'Category'
            },
            {
                path: 'subCategoryId',
                model: 'SubCategory'
            },
            {
                path: 'heathConditionId',
                model: 'HeathCondition'
            },
            {
                path: 'imageArray',
                model: 'Image'
            },
            {
                path: 'pharmaCompanyId',
                model: 'PharmaCompany'
            },
            {
                path: 'coverImage',
                model: 'Image'
            },
            ]
        }
        const searchParams = {};
        if(type != "" && type != undefined){
            searchParams['type'] = type;
        }
        if(name != "" && name != undefined){
            const user_name = await User.find( { $or:[ {'first_name':new RegExp(name,'i')}, {'last_name':new RegExp(name,'i')} ]}).distinct('_id')
            searchParams['user_id'] = {"$in":user_name}
        }
        if(phoneNumber != "" && phoneNumber != undefined){
            const user_name = await User.find( { 'phoneNumber':new RegExp(phoneNumber,'i')}).distinct('_id')
            searchParams['user_id'] = {"$in":user_name}
        }
        if(cityId != "" && cityId != undefined){
            searchParams['cityId'] = cityId
        }
        if(from && to){
            searchParams['createdAt'] = {
                $gte: new Date(from),
                $lte: new Date(to)
              }
        }
        console.log(searchParams);
        const all_orders = await Order.find(searchParams).populate('address_id').populate('user_id').populate(products_all).populate('coupon_id').sort({'updatedAt':-1}).skip(skip).limit(limit);
        return {
            data:{
                all_orders,
                total_count:all_orders.length,
                message:"All Orders"
            }
        }
    }
    this.updateStatus = async({body}) => {
        const {status,order_id,expected_delivery_date,shipped_date} = body;
        const order = await Order.findById(order_id).populate('user_id');
        if(order == null) {
            return TE("Cannot Found Order");
        }
        order.status = status;
        const user = order.user_id;
        if(status == 3){
            order.expected_delivery_date = expected_delivery_date;
        }
        if(status == 2){
            order.shipped_date = new Date();
        }
        if(status == 4){
            order.delivery_date = new Date();
        }
        await order.save();

        if(status == "3"){
            if(user.fcmToken != ""){
                const message_notification = {
                    notification: {
                      title: "Order Shipped",
                      body: "Your Order has been successfully.",
                    },
                  };
                await Notification.create({
                    user_id:user._id,
                    title:message_notification.notification.title,
                    body:message_notification.notification.body
                })
                await sendNotification(user.fcmToken,message_notification)
            }
        }
        if(status == "5"){
            if(user.fcmToken != ""){
                const message_notification = {
                    notification: {
                      title: "Order Cancelled",
                      body: "Your Order has been cancelled.",
                    },
                  };
                await Notification.create({
                    user_id:user._id,
                    title:message_notification.notification.title,
                    body:message_notification.notification.body
                })
                await sendNotification(user.fcmToken,message_notification)
            }
        }
        return {
            data: {order,message:"Update Order Status"}
        }
    }
    this.shipping = async ({body}) => {
        const {order_id,length,breadth,height,weight} = body;
        if(!order_id || !length || !breadth || !height || !weight){
            return TE('Invalid Parameter: order_id,length,breadth,height,weight is required');
        }
        const order = await Order.findById(order_id).populate('products.product').populate('address_id').populate('user_id');
        // const shipping_token = await shiprocket.getShipRocketToken();
        const ship_order = await shiprocket.shipOrder(order,length,breadth,height,weight);
        order.shiprocket_shipment_id = ship_order.shipment_id
        order.shiprocket_order_id = ship_order.order_id
        order.status = 3;
        await order.save();
        return {
            data:{
                order,
                message:"Shiprocket Shipment Added"
            }
        }
    }
    this.getShippingInformation = async ({body}) => {
        const {order_id} = body
        const order = await Order.findById(order_id);
        if(order.shiprocket_order_id == ""){
            return TE('Order is still not shipped');
        }
        const shipping_details = await shiprocket.shippingInformation(order.shiprocket_order_id,order.shiprocket_shipment_id)
        return {
            data:{
                shipping_details,
                message:"Shipping Information"
            }
        }
    }
    this.getActiveOrder = async({body,decoded}) => {
        const {skip,limit} = body;
        const user_id = decoded._id;
        const searchParams = {}
        searchParams['user_id'] = user_id
        searchParams['status'] = {"$in":[1,2,3]}
        const all_order = await getAllOrderInstance(searchParams,skip,limit);
        return {
            data:{
                all_order,
                message:"All Active Order"
            }
        }
    }
    this.getCompletedOrder = async({body,decoded}) => {
        const {skip,limit} = body;
        const user_id = decoded._id;
        const searchParams = {}
        searchParams['user_id'] = user_id
        searchParams['status'] = {"$in":[4]}
        const all_order = await getAllOrderInstance(searchParams,skip,limit);
        return {
            data:{
                all_order,
                message:"All Completed Order"
            }
        }
    }
    this.getAllOrderInstance = async(searchParams,skip,limit) => {

        const products_all = {
            path:'products.product',
            populate: [{
                path: 'cityId',
                model: 'City'
            },
            {
                path: 'categoryId',
                model: 'Category'
            },
            {
                path: 'subCategoryId',
                model: 'SubCategory'
            },
            {
                path: 'heathConditionId',
                model: 'HeathCondition'
            },
            {
                path: 'imageArray',
                model: 'Image'
            },
            {
                path: 'pharmaCompanyId',
                model: 'PharmaCompany'
            },
            {
                path: 'coverImage',
                model: 'Image'
            },
            ]
        }
        return await Order.find(searchParams).populate('address_id').populate('user_id').populate(products_all).populate('coupon_id').sort({'updatedAt':-1}).skip(skip).limit(limit);
    }
    this.getCanceledOrder = async({body,decoded}) => {
        const {skip,limit} = body;
        const user_id = decoded._id;
        const searchParams = {}
        searchParams['user_id'] = user_id
        searchParams['status'] = {"$in":[5,6]}
        const all_order = await getAllOrderInstance(searchParams,skip,limit);
        return {
            data:{
                all_order,
                message:"All Canceled Order"
            }
        }
    }
    this.cancelOrder = async({body,decoded}) => {
        const {order_id,cancel_reason} = body;
        const user_id = decoded._id;
        const user = await User.findById(user_id);
        if(!order_id) {
            return TE("Invalid Order ID");
        }
        const order = await Order.findById(order_id)
        if(order.status == 4){
            return TE("Order cannot be canceled");
        }
        if(order.status == 5 || order.status == 6){
            return TE("Order Already Cancel");
        }
        var updatedOrderStatus = await makeInstance(Order,{status:5,cancel_reason},order_id);
        if(updatedOrderStatus.type == 1){
            await addAmountRefund(user_id,order_id,updatedOrderStatus.amount.grandAmount)
            updatedOrderStatus = await makeInstance(Order,{status:6,cancel_reason},order_id);
        }
        if(user.fcmToken != ""){
            const message_notification = {
                notification: {
                  title: "Order Cancelled",
                  body: "Your Order has been cancelled.",
                },
              };
            await Notification.create({
                user_id:user._id,
                title:message_notification.notification.title,
                body:message_notification.notification.body
            })
            await sendNotification(user.fcmToken,message_notification)
        }
        return {
            data:{
                updatedOrderStatus,
                message:"Cancel Order"
            }
        }
    }
    this.getEachOrder = async({params,decoded}) => {
        const user_id = decoded._id
        const {order_id} = params;
        const searchParams = {}
        searchParams['_id'] = order_id;
        const order = await getAllOrderInstance(searchParams);
        var shipping = null;
        if(order.shiprocket_order_id != ""){
            shipping = await shiprocket.shippingInformation(order[0].shiprocket_order_id);
        }
        return {
            data:{
                order:order[0],
                shipping,
                message:"Each Order"
            }
        }
    }
    return this;
  })();