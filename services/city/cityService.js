const City = require('../../models/city')
const CitySetting = require('../../models/citySetting')
const DefaultCity = require('../../models/default_city')



module.exports = (function () {
    /**
     * @params body
     * @For create new City
     */
    this.makeCityInstance = async(data,id) => {
        let city;
        if(id == null){
            city = new City;
        }else{
            city = await City.findById(id);
            if(city == null){
                return TE('Cannot Found City')
            }
        }
        Object.keys(data).map((key,) => {
            city[key] = data[key];
        })
        return await city.save();
    }
    this.createCity = async({body}) => {
        const {s_no,title,latitute,longitude,point,status} = body;
        if(!s_no || !title || !latitute || !longitude || !point){
            return TE("Invalid Parameter: s_no,title,latitute,longitude,point,status is required");
        }
        // check if title Exists
        if(await alreadyExistTitle(City,title)){
            return TE("City Title Already Exists");
        }
        if(await alreadyExist(City,{s_no:s_no})){
            return TE("City S_No Already Exists");
        }
        const cityData = {s_no,title,latitute,longitude,point,status}
        const cityInstance = await makeCityInstance(cityData,null);
        city_setting = makeNewCitySettingIFNotExists(cityInstance._id);   
        return {
            data: {
                message:"New City Added",
                cityInstance
            }
        }
    }

    this.updateCity = async({body}) => {
        const {s_no,title,latitute,longitude,point,status,city_id} = body;
        if(!s_no || !title || !latitute || !longitude || !point || !city_id){
            return TE("Invalid Parameter: s_no,title,latitute,longitude,point,status,city_id is required");
        }
        const cityData = {s_no,title,latitute,longitude,point,status}
        const cityInstance = await makeCityInstance(cityData,city_id);
        return {
            data: {
                message:"City Updated",
                cityInstance
            }
        }
    }
    this.fetchCity = async({body}) => {
        const {status,skip,limit} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        const all_data = await City.find(searchParams).sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All City",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params city_id
     * @For delete the city
     */
    this.deleteCity = async({params}) => {
        const {city_id} = params
        const city = await City.findById(city_id)
        if(city == null) {
            return TE('Cannot Find City');
        }
        await city.delete();
        return {
            data:{
                message:"City Deleted",
            }
        }
    }

    /**
     * @params body
     * @For create or update city 
     */
    this.createUpdateCitySetting = async({body}) => {
        const {city_id,is_cod,minimum_order_price,delivery_price,free_delivery_order_price,payment_gateway_percentage} = body;
        if(!city_id){
            return TE('Invalid Parameter: city_id');
        }
        let fndupdoptions = {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        }
        const city_setting = await CitySetting.findOneAndUpdate({city_id: city_id}, {is_cod,minimum_order_price,delivery_price,free_delivery_order_price,payment_gateway_percentage}, fndupdoptions).select('-id -_v').lean();
        return {
            data:{
                city_setting,
                message:"City Setting Updated"
            }
        }
    }

    this.makeNewCitySettingIFNotExists = async(city_id) => {
        let fndupdoptions = {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        }
        city_setting = await CitySetting.findOneAndUpdate({city_id: city_id}, {}, fndupdoptions).select('-id -_v').lean();
        return city_setting;
    }
    this.getCitySetting = async({params}) => {
        const {city_id} = params;
        var city_setting = await CitySetting.findOne({city_id});
        if(city_setting == null){
            city_setting = makeNewCitySettingIFNotExists(city_id);   
        }
        return {
            data:{
                city_setting,
                message:"Get City Setting"
            }
        }
    }
    this.updateCityStatus = async({body}) => {
        const {city_id,status} = body
        console.log(body)
        if(!city_id){
            return TE("Invalid Parameter: city_id,status is required");
        }
        const cityData = {status}
        const cityInstance = await makeCityInstance(cityData,city_id);
        return {
            data: {
                message:"City Status Updated",
                cityInstance
            }
        }
    }
    this.getDefaultCity = async ({}) => {
        var default_city = await DefaultCity.findOne();
        if(default_city == null){
            return TE("Default City is not set")
        }
        var default_city = await DefaultCity.findOne().populate('city_id');
        return {
            data:{
                default_city,
                "message":"Default City"
            }
        };
    }
    this.updateDefaultCity = async({body}) => {
        const {city_id} = body;
        var default_city = await DefaultCity.findOne();
        if(default_city){
            default_city.city_id = city_id
            await default_city.save();
        }else{
            default_city = await DefaultCity.create({
                city_id
            })
        }
        return {
            data:{
                default_city,
                "message":"Default City Updated"
            }
        };
    
    }
    this.getAllView = async({body}) => {
        const {status,skip,limit} = body;
        const searchParams = {};
        searchParams['status'] = true;
        const all_data = await City.find(searchParams).sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All City",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    return this;
  })();