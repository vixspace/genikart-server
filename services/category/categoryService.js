const Category = require('../../models/category')
const SubCategory = require('../../models/subCategory')



module.exports = (function () {
    this.create = async({body}) => {
        const {s_no,name,logo,status} = body;
        if(!s_no || !name ){
            return TE("Invalid Parameter: s_no,name,logo,status is required");
        }
        // check if title Exists
        if(await alreadyExistTitle(Category,name)){
            return TE("Category Already Exists");
        }

        if(await alreadyExist(Category,{s_no:s_no})){
            return TE("City S_No Already Exists");
        }

        const data = {s_no,name,logo,status}
        const instance = await makeInstance(Category,data,null);
        return {
            data: {
                message:"New Category Added",
                categoryInstance:instance
            }
        }
    }

    this.update = async({body}) => {
        const {s_no,name,logo,status,category_id} = body;
        if(!s_no || !name || !logo || !category_id){
            return TE("Invalid Parameter: s_no,name,logo,category_id is required");
        }
        const data = {s_no,name,logo,status}
        const instance = await makeInstance(Category,data,category_id);
        return {
            data: {
                message:" Updated",
                categoryCompanyInstance:instance
            }
        }
    }
    this.getAll = async({body}) => {
        const {status,name,skip,limit} = body;
        const searchParams = {};
        if(status != "" && status != undefined){
            searchParams['status'] = status;
        }
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        const all_data = await Category.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Category",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    /**
     * @params pharma_id
     * @For delete the pharma
     */
    this.remove = async({params}) => {
        const {id} = params
        const category = await Category.findById(id)
        if(category == null) {
            return TE('Cannot Find Category');
        }
        await category.delete();
        return {
            data:{
                message:"Category Deleted",
            }
        }
    }

    this.updateStatus = async({body}) => {
        const {category_id,status} = body
        if(!category_id && status != undefined){
            return TE("Invalid Parameter: category_id,status is required");
        }
        const data = {status}
        const instance = await makeInstance(Category,data,category_id);
        return {
            data: {
                message:"Category Status Updated",
                categoryInstance:instance
            }
        }
    }
    this.getCategoryView = async({body}) => {
        const {status,name,skip,limit} = body;
        const searchParams = {};
        if(name != "" && name != undefined){
            searchParams['name'] = new RegExp(name,'i');
        }
        searchParams['status'] = true;
        const all_data = await Category.find(searchParams).populate('logo').sort({'s_no':1}).skip(skip).limit(limit)
        return {
            data:{
                message:"All Category",
                all_data,
                total_count:all_data.length,
                skip,limit
            }
        }
    }
    return this;
  })();