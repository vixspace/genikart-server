const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const CartSchema = new Schema(
  {
    product_id:{
      type:Schema.Types.ObjectId,
      ref:'Product'
    },
    quantity:{
      type:Number,
      required:[true,"Product Quantity is required"]
    },
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User'
    },
    order_id:{
      type:Schema.Types.ObjectId,
      ref:'Order'
    },
    in_cart:{
      type: Boolean, default: true,
    },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Cart", CartSchema);
