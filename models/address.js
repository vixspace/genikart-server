const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const addressSchema = new Schema(
  {
    name: {type:String,default:''},
    address: {type:String,default:''},
    landmark: {type:String,default:''},
    city: {type:String,default:''},
    state: {type:String,default:''},
    pincode: {type:String,default:''},
    mobile_number: {type:String,default:''},
    email: {type:String,default:''},
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User'
    },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Address", addressSchema);
