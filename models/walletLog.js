const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const walletLogSchema = new Schema(
  {
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User',
      required:[true,"User Id is required"]
    },
    debit:{
      type:Number,default:0
    },
    credit:{ type:Number,default:0 },
    description:{ type:String,default:''},
    transaction_id:{type:String,default:''},
    razorpay_id:{type:String,default:''},
    order_id:{
      type:Schema.Types.ObjectId,
      ref:'Order',
      default:null
    },
    status:{
      type:Boolean, default: true
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("WalletLog", walletLogSchema);
