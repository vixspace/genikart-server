const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const CitySchema = new Schema(
  {
    s_no: {
      type: Number,
      default:""
    },
    title:{
      type: String,
      default:"",
      unique: [true, "City Title is already exists."],
    },
    latitute:{
      type: Number, default: 0
    },
    longitude:{
      type: Number, default: 0
    },
    point: { type: [Number], index: '2d' },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("City", CitySchema);
