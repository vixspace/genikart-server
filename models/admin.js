const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminSchema = new Schema(
  {
    name:{
        type: String,
        require:[true,"Admin Name is Required"]
    },
    email:{
        type: String,
        require:[true,"Admin Email is Required"],
        unique: true
    },
    hash:{
        type: String,
        require:[true,"Admin Hash is Required"]
    }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Admin", adminSchema)