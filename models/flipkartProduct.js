const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const flipkartProductSchema = new Schema(
  {
    analyticsData: {
        "category":{
          type:String,
        },
        "subCategory":{
          type:String,
        },
        "superCategory":{
          type:String,
        },
        "vertical":{
          type:String,
        }
    },
    availability:{
      type:Boolean,
    },
    baseUrl:{
      type:String,
    },
    id:{
      type:String,
    },
    itemId:{
      type:String,
    },
    keySpecs:[{
      type:String,
    }],
    listingId:{
      type:String,
    },
    images:[{
      type:String,
    }],
    pricing:{
      "deliveryCharge":{
        type:String,
      },
      "discountAmount":{
        type:String,
      },
      "extraDiscount":{
        type:String,
      },
      "finalPrice":{
        type:String,
      }
    },
    mrp:{
      type:String
    },
    productBrand:{
      type:String
    },
    rating:{
      type:String
    },
    smartUrl:{
      type:String
    },
    titles:{
      "coSubtitle":{
        type:String,
      },
      "newTitle":{
        type:String,
      },
      "superTitle":{
        type:String,
      },
      "title":{
        type:String,
      }
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("FlipkartProduct", flipkartProductSchema);
