const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const ImageSchema = new Schema(
  {
    name: { type: String },
    path: { type: String },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Image", ImageSchema);
