const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const userOtpSchema = new Schema(
  {
    phoneNumber:{
      type:String,
      required:true,
    },
    OTP:{
      type:String,
      required:true,
    },
    latest:{
      type:String,
      default:true
    },
    is_verified:{
      type:Boolean,default:false
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("UserOtp", userOtpSchema);
