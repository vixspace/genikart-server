const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const defaultCitySchema = new Schema(
  {
    city_id:{
      type:Schema.Types.ObjectId,
      ref:'City',
    }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("DefaultCity", defaultCitySchema)