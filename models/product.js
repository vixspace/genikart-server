const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const ProductSchema = new Schema(
  {
    s_no: { type: String },
    name: {type:String, default:''},
    coverImage:
    {
        type:Schema.Types.ObjectId,
        ref:'Image',
        default:null
    },
    imageArray:[
      {
        type:Schema.Types.ObjectId,
        ref:'Image',
        default:null
      }
    ],
    pharmaCompanyId:{
      type:Schema.Types.ObjectId,
      ref:'PharmaCompany',
      default:null
    },
    about:{type:String, default:''},
    composition:{type:String, default:''},
    quantity:{type:Number, default:0},
    max_retail_price:{type:Number, default:0},
    selling_price:{type:Number, default:0},
    discount_amount:{type:Number, default:0},
    expiry_date:{type:Date},
    cityId:[
      {
        type:Schema.Types.ObjectId,
        ref:'City',
      }
    ],
    categoryId:[
      {
        type:Schema.Types.ObjectId,
        ref:'Category',
      }
    ],
    subCategoryId:[
      {
        type:Schema.Types.ObjectId,
        ref:'SubCategory',
      }
    ],
    heathConditionId:[
      {
        type:Schema.Types.ObjectId,
        ref:'HeathCondition',
      }
    ],
    weight:{
      type:Number, default:0
    },
    width:{
      type:Number, default:0
    },
    length:{
      type:Number, default:0
    },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Product", ProductSchema);
