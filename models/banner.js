const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bannerSchema = new Schema(
  {
    s_no:{
      type: Number,
      require:[true,"S No is Required"]
    },
    name:{
        type: String,
        require:[true,"Name is Required"]
    },
    url:{
      type:String,
      default:""
    },
    image_id:{
      type:Schema.Types.ObjectId,
      ref:'Image',
    },
    status:{type: Boolean, default: true }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Banner", bannerSchema)