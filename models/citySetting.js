const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const CitySettingSchema = new Schema(
  {
    city_id:{
      type:Schema.Types.ObjectId,
      ref:'City'
    },
    is_cod: { type: Boolean, default: true },
    minimum_order_price: { type: Number, default: 300 },
    delivery_price: { type: Number, default: 50 },
    free_delivery_order_price: { type: Number, default: 700 },
    payment_gateway_percentage: { type: Number, default: 3.6 },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("CitySetting", CitySettingSchema);
