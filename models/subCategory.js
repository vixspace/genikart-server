const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const subCategorySchema = new Schema(
  {
    s_no: { type: String },
    category_id: { 
      type:Schema.Types.ObjectId,
      ref:'Category',
      default:null
    },
    name: {type:String},
    logo: { 
      type:Schema.Types.ObjectId,
      ref:'Image',
      default:null
    },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("SubCategory", subCategorySchema);
