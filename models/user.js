const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const userSchema = new Schema(
  {
    first_name: {
      type: String,
      default:""
    },
    last_name: {
      type: String,
      default:""
    },
    business_name: { type:String, default:"" },
    business_address:{ type:String, default:""},
    pincode:{type:String, default:""},
    profilePic:{
      type:Schema.Types.ObjectId,
      ref:'Image',
      default:null
    },
    email:{
      type: String,
      default:""
    },
    phoneNumber:{
      type: String,
      unique: [true, "Phone Number is already exists."],
      default:""
    },
    type:{
      type:Number,
      default:0,
      //1 Retail 2=> Wholesale 3=> Doctor
    },
    cityId:{
      type:Schema.Types.ObjectId,
      ref:'City',
    },
    fcmToken: {
      type: String,
      default:""
    },
    latitute:{
      type: Number, default: 0
    },
    longitude:{
      type: Number, default: 0
    },
    googleAddress:{
      type:String, default:''
    },
    gst_number:{
      type:String, default:""
    },
    drug_license_number:{ type:String, default:"" },
    gst_image:{ type:String, type:Schema.Types.ObjectId, ref:'Image'},
    drug_image:{ type:String, type:Schema.Types.ObjectId, ref:'Image'},
    status:{
      type:Number,
      default:0, 
      // 0 => Not Submitted
      // 1 => Submitted
      // 2 => Approval
      // 3 => Blocked
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("User", userSchema);
