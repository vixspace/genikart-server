const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const orderSchema = new Schema(
  {
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User'
    },
    products:[
      {
        product:{
          type:Schema.Types.ObjectId,
          ref:'Product'
        },
        quantity:{
          type:Number,
          required:[true,"Product Quantity is required"]
        },
      }
    ],
    address_id:{type:Schema.Types.ObjectId,ref:'Address'},
    cityId:{type:Schema.Types.ObjectId,ref:'City'},
    coupon_id:{type:Schema.Types.ObjectId,ref:'Coupon'},
    amount:{
      cartAmount:{type:Number,default:0},
      couponAmount:{type:Number,default:0},
      delivery_amount:{type:Number,default:0},
      gateWayAmount:{type:Number,default:0},
      grandAmount:{type:Number,default:0},
    },
    type:{type:Number,default:1},
    /**
     * 1) COD
     * 2) Wallet
     */
    status: {type:Number, default: 1 }, 
    /**
     * 1) Placed
     * 2) Approval
     * 3) Shipped
     * 4) Deliverd
     * 5) Cancelled
     * 6) Refunded
     */
    shiprocket_shipment_id:{type:String,default:''},
    shiprocket_order_id:{type:String,default:''},
    cancel_reason:{type:String,default:''},
    
    expected_delivery_date:{type:Date,default:''},
    shipped_date:{type:Date,default:''},
    shipped_date:{type:Date,default:''},
    delivery_date:{type:Date,default:''},
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Order", orderSchema);
