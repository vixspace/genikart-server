const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const razorpayLogSchema = new Schema(
  {
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User',
      required:[true,"User Id is required"]
    },
    wallet_id:{
      type:Schema.Types.ObjectId,
      ref:'WalletLog',
      default:null
    },
    amount:{
      type:Number,
      required:[true,"Amount is required"]
    },
    receipt_id:{
      type:String,
      required:[true,"Receipt Id is required"]
    },
    order_id:{
      type:String,
      required:[true,"Order Id is required"]
    },
    razorpay_id:{
      type:String,
      default:null
    },
    status:{
      type:Number,
      default:0,
      /**
       * 0- Started
       * 1- Success
       * 2- Failure
       */
    },
    callbackJson: {
      type: String,
      get: function(data) {
        try { 
          return JSON.parse(data);
        } catch(error) { 
          return data;
        }
      },
      set: function(data) {
        return JSON.stringify(data);
      }
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("RazorpayLog", razorpayLogSchema);
