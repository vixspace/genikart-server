const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const categorySchema = new Schema(
  {
    s_no: { type: String },
    name: {type:String},
    logo: { 
      type:Schema.Types.ObjectId,
      ref:'Image',
      default:null
    },
    status: { type: Boolean, default: true },
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Category", categorySchema);
