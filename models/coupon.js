const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const couponSchema = new Schema(
  {
    code:{
        type: String,
        require:[true,"Code is Required"],
    },
    description:{
        type: String,
        default:""
    },
    terms_condition:{
        type: String,
        default:""
    },
    start_date_time:{
      type:Date,
      default: null
    },
    end_date_time:{
      type:Date,
      default: null
    },
    discount_percentage:{
      type:Number,
      default:0
    },
    minimum_billing_amount:{
      type:Number,
      default:0
    },
    max_user_applicable_orders:{
      type:Number,
      default:0
    },
    max_discount_amount:{ 
      type:Number,
      default:0
    },
    image_id:{
      type:Schema.Types.ObjectId,
      ref:'Image',
    },
    status:{type: Boolean, default: true }
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("Coupon", couponSchema)