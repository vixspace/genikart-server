const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const versionSettingSchema = new Schema(
  {
    android_version: { type: Number, default: 1 },
    ios_version: { type: Number, default: 1 },
  },
  { timestamps: true },
  { minimize: false }
);


module.exports = mongoose.model("VersionSetting", versionSettingSchema)