const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const notificationSchema = new Schema(
  {
    user_id:{
      type:Schema.Types.ObjectId,
      ref:'User'
    },
    title:{
      type:String,
      required:[true,"Title is required"]
    },
    body:{
      type:String,
      required:[true,"Body is required"]
    },
    status:{
      type:Number,
      default:1, 
      // 1 => Display to Notification Screen
      // 2 => Hide from Notification Screen
    }
  },
  { timestamps: true },
  { minimize: false }
);



module.exports = mongoose.model("Notification", notificationSchema);
