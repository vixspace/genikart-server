const {
  getDefaultCity,
  updateDefaultCity,
} = require("../services/city/cityService")

module.exports = {    

  get: async(req, res) => {
    try {
      ReS(res,await getDefaultCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "defaultCityController Controller >>> get method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await updateDefaultCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "defaultCityController Controller >>> update method");
    }
  }
}