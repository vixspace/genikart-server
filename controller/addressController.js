const {
  create,
  getAll,
  edit,
  remove
} = require("../services/address/addressService")

module.exports = {    
  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> create method");
    }
  },
  edit: async(req, res) => {
    try {
      ReS(res,await edit(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> edit method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getAll method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> remove method");
    }
  }
}