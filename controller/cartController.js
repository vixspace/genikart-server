const {
  addUpdateToCart,
  getCartItem
} = require("../services/cart/cartService")

module.exports = {    

  addUpdateToCart: async(req, res) => {
    try {
      ReS(res,await addUpdateToCart(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> addUpdateToCart method");
    }
  },
  getCartItem: async(req, res) => {
    try {
      ReS(res,await getCartItem(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> getCartItem method");
    }
  }
}