const {
    callback
} = require("../services/razorpay/razorpayService")
  

module.exports = {  
    
    /**
     *  This handler is use creating the new feature service  
     */
    callback: async (req,res) => {
        try {
            ReS(res,await callback(req),200)
        } catch (error) {
          ReE(res, error, 422, "razorpayController >>> callback method");
        }
    },

}