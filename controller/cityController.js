const {
  createCity,
  updateCity,
  fetchCity,
  deleteCity,
  createUpdateCitySetting,
  getCitySetting,
  updateCityStatus,
  getAllView
} = require("../services/city/cityService")

module.exports = {    

  createCity: async(req, res) => {
    try {
      ReS(res,await createCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> createCity method");
    }
  },
  updateCity: async(req, res) => {
    try {
      ReS(res,await updateCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> updateCity method");
    }
  },
  fetchCity: async(req, res) => {
    try {
      ReS(res,await fetchCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> fetchCity method");
    }
  },
  deleteCity: async(req, res) => {
    try {
      ReS(res,await deleteCity(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> deleteCity method");
    }
  },
  createUpdateCitySetting: async(req, res) => {
    try {
      ReS(res,await createUpdateCitySetting(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> createUpdateCitySetting method");
    }
  },
  getCitySetting: async(req, res) => {
    try {
      ReS(res,await getCitySetting(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> getCitySetting method");
    } 
  },
  updateCityStatus: async(req, res) => {
    try {
      ReS(res,await updateCityStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> updateCityStatus method");
    }
  },
  getAllView: async(req, res) => {
    try {
      ReS(res,await getAllView(req),200)
    } catch (error) {
      ReE(res, error, 422, "cityController Controller >>> getAllView method");
    }
  }
}