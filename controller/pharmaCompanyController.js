const {
  createPharma,
  updatePharma,
  getAllPharma,
  deletePharma,
  updatePharmaStatus,
  getAllView
} = require("../services/pharma_company/pharmaService")

module.exports = {    

  createPharma: async(req, res) => {
    try {
      ReS(res,await createPharma(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> createPharma method");
    }
  },
  updatePharma: async(req, res) => {
    try {
      ReS(res,await updatePharma(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> updatePharma method");
    }
  },
  getAllPharma: async(req, res) => {
    try {
      ReS(res,await getAllPharma(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> getAllPharma method");
    }
  },
  deletePharma: async(req, res) => {
    try {
      ReS(res,await deletePharma(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> deletePharma method");
    }
  },
  updatePharmaStatus: async(req, res) => {
    try {
      ReS(res,await updatePharmaStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> updatePharmaStatus method");
    }
  },
  getAllView: async(req, res) => {
    try {
      ReS(res,await getAllView(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> getAllView method");
    }   
  }
}