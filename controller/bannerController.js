const {
  create,
  update,
  getAll,
  remove,
  updateStatus,
  getBannerView
} = require("../services/banner/bannerService")

module.exports = {    

  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> create method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await update(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> update method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> getAll method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> remove method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> updateStatus method");
    }
  },
  getIndividualProduct: async(req, res) => {
    try {
      ReS(res,await getIndividualProduct(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> getIndividualProduct method");
    }
  },
  getBannerView: async(req, res) => {
    try {
      ReS(res,await getBannerView(req),200)
    } catch (error) {
      ReE(res, error, 422, "bannerController Controller >>> getBannerView method");
    }
  }
}