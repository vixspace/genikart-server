const {
  create,
  update,
  getAll,
  remove,
  updateStatus,
  getCategoryView
} = require("../services/category/categoryService")

module.exports = {    

  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> create method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await update(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> update method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> getAll method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> remove method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> updateStatus method");
    }
  },
  getCategoryView: async(req, res) => {
    try {
      ReS(res,await getCategoryView(req),200)
    } catch (error) {
      ReE(res, error, 422, "categoryController Controller >>> getCategoryView method");
    }
  },
}