const {
    upload
} = require("../services/utils/otpService")
  

module.exports = {  
    
    /**
     *  This handler is use creating the new feature service  
     */
    upload: async (req,res) => {
        try {
            ReS(res,await upload(req),200)
        } catch (error) {
          ReE(res, error, 422, "imageUploadController >>> upload method");
        }
    },

}