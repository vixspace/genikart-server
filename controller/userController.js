const {
  sendOtp,
  verifyOtp,
  updateProfile,
  submitForApproval,
  getUserInformation,
  globalSearch,
  addAmounToWallet,
  getAllWalletLog,
  getAll,
  getIndividualUser,
  updateUserStatus,
  updateFcmToken,
  getCanceledOrder,
  generateRazorpayTransaction,
  updateCityIdByLatLong,
  getAllNotifications,
  clearNotification,
  flipkartProductAdd
} = require("../services/user/userService")

module.exports = {    

  sendOtp: async(req, res) => {
    try {
      ReS(res,await sendOtp(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> sendOtp method");
    }
  },
  verifyOtp: async(req, res) => {
    try {
      ReS(res,await verifyOtp(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> verifyOtp method");
    }
  },
  updateProfile: async(req, res) => {
    try {
      ReS(res,await updateProfile(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> updateProfile method");
    }
  },
  submitForApproval: async(req, res) => {
    try {
      ReS(res,await submitForApproval(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> submitForApproval method");
    }
  },
  getUserInformation: async(req, res) => {
    try {
      ReS(res,await getUserInformation(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getUserInformation method");
    }
  },
  globalSearch: async(req, res) => {
    try {
      ReS(res,await globalSearch(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> globalSearch method");
    }
  },
  addAmounToWallet: async(req, res) => {
    try {
      ReS(res,await addAmounToWallet(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> addAmounToWallet method");
    }
  },
  getAllWalletLog: async(req, res) => {
    try {
      ReS(res,await getAllWalletLog(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getAllWalletLog method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getAll method");
    }
  },
  getIndividualUser: async(req, res) => {
    try {
      ReS(res,await getIndividualUser(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getIndividualUser method");
    }
  },
  updateUserStatus: async(req, res) => {
    try {
      ReS(res,await updateUserStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> updateUserStatus method");
    }
  },
  updateFcmToken: async(req, res) => {
    try {
      ReS(res,await updateFcmToken(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> updateFcmToken method");
    }
  },
  getCanceledOrder: async(req, res) => {
    try {
      ReS(res,await getCanceledOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getCanceledOrder method");
    }
  },
  generateRazorpayTransaction: async(req, res) => {
    try {
      ReS(res,await generateRazorpayTransaction(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> generateRazorpayTransaction method");
    }
  },
  updateCityIdByLatLong: async(req, res) => {
    try {
      ReS(res,await updateCityIdByLatLong(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> updateCityIdByLatLong method");
    } 
  },
  getAllNotifications: async(req, res) => {
    try {
      ReS(res,await getAllNotifications(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> getAllNotifications method");
    } 
  },
  clearNotification: async(req, res) => {
    try {
      ReS(res,await clearNotification(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> clearNotification method");
    } 
  },
  flipkartProductAdd: async(req, res) => {
    try {
      ReS(res,await flipkartProductAdd(req),200)
    } catch (error) {
      ReE(res, error, 422, "userController Controller >>> flipkartProductAdd method");
    } 
  },
}