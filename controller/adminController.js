const {
  loginAdmin,
  createAdmin,
  updateAdminPassword,
  updateAdmin,
  getAllAdmin,
  deleteAdmin,
  getVersionSetting,
  updateVersionSetting
} = require("../services/admin/adminService")

module.exports = {    


  /**
   *  This handler is use for login of admin user
   */
  loginAdmin : async(req, res) => {
      try {
          ReS(res,await loginAdmin(req),200)
      } catch (error) {
        ReE(res, error, 422, "adminController Controller >>> loginAdmin method");
      }
  },
  createAdmin: async(req, res) => {
    try {
      ReS(res,await createAdmin(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> createAdmin method");
    }
  },
  updateAdminPassword: async(req, res) => {
    try {
      ReS(res,await updateAdminPassword(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> updateAdminPassword method");
    }
  },
  updateAdmin: async(req, res) => {
    try {
      ReS(res,await updateAdmin(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> updateAdmin method");
    }
  },
  getAllAdmin: async(req, res) => {
    try {
      ReS(res,await getAllAdmin(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> getAllAdmin method");
    }
  },
  
  deleteAdmin: async(req, res) => {
    try {
      ReS(res,await deleteAdmin(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> deleteAdmin method");
    }
  },
  getVersionSetting: async(req, res) => {
    try {
      ReS(res,await getVersionSetting(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> getVersionSetting method");
    }
  },
  updateVersionSetting: async(req, res) => {
    try {
      ReS(res,await updateVersionSetting(req),200)
    } catch (error) {
      ReE(res, error, 422, "adminController Controller >>> updateVersionSetting method");
    }
  }
}