const {
  create,
  update,
  getAll,
  getAllView,
  remove,
  updateStatus,
  getIndividualProduct,
  getDiscountedProductView
} = require("../services/product/productService")

module.exports = {    

  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> create method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await update(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> update method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> getAll method");
    }
  },
  getAllView: async(req, res) => {
    try {
      ReS(res,await getAllView(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> getAllView method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> remove method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> updateStatus method");
    }
  },
  getIndividualProduct: async(req, res) => {
    try {
      ReS(res,await getIndividualProduct(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> getIndividualProduct method");
    }
  },
  getDiscountedProductView: async(req, res) => {
    try {
      ReS(res,await getDiscountedProductView(req),200)
    } catch (error) {
      ReE(res, error, 422, "productController Controller >>> getDiscountedProductView method");
    } 
  }
}