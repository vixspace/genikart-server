const {
  placeOrder,
  summary,
  getAllUserOrder,
  getAll,
  updateStatus,
  shipping,
  getShippingInformation,
  getActiveOrder,
  getCompletedOrder,
  getCanceledOrder,
  cancelOrder,
  getEachOrder
} = require("../services/order/orderService")

module.exports = {    

  placeOrder: async(req, res) => {
    try {
      ReS(res,await placeOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> placeOrder method");
    }
  },
  summary: async(req, res) => {
    try {
      ReS(res,await summary(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> summary method");
    }
  },
  getAllUserOrder: async(req, res) => {
    try {
      ReS(res,await getAllUserOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getAllUserOrder method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getAll method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> updateStatus method");
    }
  },
  shipping: async(req, res) => {
    try {
      ReS(res,await shipping(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> shipping method");
    }
  },
  getShippingInformation: async(req, res) => {
    try {
      ReS(res,await getShippingInformation(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getShippingInformation method");
    }
  },
  getActiveOrder: async(req, res) => {
    try {
      ReS(res,await getActiveOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getActiveOrder method");
    }
  },
  getCompletedOrder: async(req, res) => {
    try {
      ReS(res,await getCompletedOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getCompletedOrder method");
    }
  },
  getCanceledOrder: async(req, res) => {
    try {
      ReS(res,await getCanceledOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getCanceledOrder method");
    }
  },
  cancelOrder: async(req, res) => {
    try {
      ReS(res,await cancelOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> cancelOrder method");
    }
  },
  getEachOrder: async(req, res) => {
    try {
      ReS(res,await getEachOrder(req),200)
    } catch (error) {
      ReE(res, error, 422, "orderController Controller >>> getEachOrder method");
    }
  }
}