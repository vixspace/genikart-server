const {
  create,
  update,
  getAll,
  remove,
  updateStatus
} = require("../services/heath_condition/heathConditionService")

module.exports = {    

  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "subCategoryController Controller >>> create method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await update(req),200)
    } catch (error) {
      ReE(res, error, 422, "subCategoryController Controller >>> update method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "subCategoryController Controller >>> getAll method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "subCategoryController Controller >>> remove method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "subCategoryController Controller >>> updateStatus method");
    }
  }
}