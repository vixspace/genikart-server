const {
  create,
  update,
  getAll,
  remove,
  updateStatus,
  getAllView
} = require("../services/coupon/couponService")

module.exports = {    

  create: async(req, res) => {
    try {
      ReS(res,await create(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> create method");
    }
  },
  update: async(req, res) => {
    try {
      ReS(res,await update(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> update method");
    }
  },
  getAll: async(req, res) => {
    try {
      ReS(res,await getAll(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> getAll method");
    }
  },
  remove: async(req, res) => {
    try {
      ReS(res,await remove(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> remove method");
    }
  },
  updateStatus: async(req, res) => {
    try {
      ReS(res,await updateStatus(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> updateStatus method");
    }
  },
  getAllView: async(req, res) => {
    try {
      ReS(res,await getAllView(req),200)
    } catch (error) {
      ReE(res, error, 422, "couponController Controller >>> getAllView method");
    }
  }
}