var admin = require("firebase-admin");

var serviceAccount = require("./genikart-firebase");
const dbUrl = "https://genikart-4e819-default-rtdb.firebaseio.com"

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: dbUrl
})

module.exports.admin = admin