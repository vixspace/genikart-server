const express = require("express");
const router = express.Router();
const user = require("../controller/userController");
const userToken = require("../middleware/checkToken")
const banner = require("../controller/bannerController");
const category = require("../controller/categoryController");
const sub_category = require("../controller/subCategoryController");
const pharma_company = require("../controller/pharmaCompanyController");
const products = require("../controller/productController");
const cart = require("../controller/cartController");
const defaultCity = require("../controller/defaultCityController");
const address = require("../controller/addressController");
const order = require("../controller/orderController");
const coupon = require("../controller/couponController");
const city = require("../controller/cityController");




router.post('/send-otp',user.sendOtp)
router.post('/verify-otp',user.verifyOtp)
router.put('/',userToken,user.updateProfile)
router.put('/update-city-by-latlong',userToken,user.updateCityIdByLatLong)

router.get('/',userToken,user.getUserInformation)
router.post('/global-search',userToken,user.globalSearch)
router.put('/submit-for-approval',userToken,user.submitForApproval)


router.get('/banner',userToken,banner.getBannerView)
router.post('/category',userToken,category.getCategoryView)
router.post('/sub_category',userToken,sub_category.getAllView)
router.post('/pharma_company',userToken,pharma_company.getAllView)
router.post('/product',userToken,products.getAllView)
router.post('/top-deal-products',userToken,products.getDiscountedProductView)

router.get('/product/:id',userToken,products.getIndividualProduct)



router.post('/cart',userToken,cart.addUpdateToCart)
router.get('/cart',userToken,cart.getCartItem)


router.post('/address',userToken,address.create)
router.post('/address/fetch',userToken,address.getAll)
router.put('/address',userToken,address.edit)
router.delete('/address/:id',userToken,address.remove)

router.get('/default-city',userToken,defaultCity.get)
router.get('/all-citys',userToken,city.getAllView)


router.post('/add-amount-to-wallet',userToken,user.addAmounToWallet);

router.post('/order/place',userToken,order.placeOrder)
router.post('/order/summary',userToken,order.summary)

router.post('/coupon',userToken,coupon.getAllView)
router.post('/wallet-log',userToken,user.getAllWalletLog)

router.post('/orders/active',userToken,order.getActiveOrder)
router.post('/orders/completed',userToken,order.getCompletedOrder)
router.post('/orders/canceled',userToken,order.getCanceledOrder)
router.put('/orders/cancel-order',userToken,order.cancelOrder)
router.get('/orders/:order_id',userToken,order.getEachOrder)



router.put('/update-fcmToken',userToken,user.updateFcmToken)
router.post('/razorpay-get-transaction',userToken,user.generateRazorpayTransaction)

router.post('/notifications',userToken,user.getAllNotifications)
router.put('/clear-notifications',userToken,user.clearNotification)





module.exports = router
