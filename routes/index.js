const express = require("express");
const router = express.Router();
const adminRouter = require("./admin");
const userRouter = require("./user");

const {upload} = require("../controller/imageUploadController")
const {flipkartProductAdd} = require('../controller/userController')
const razorpayController = require("../controller/razorpayController")

router.use("/admin", adminRouter);
router.use("/user", userRouter);

router.post("/upload",upload)
router.post("/razorpay/callback",razorpayController.callback)
router.post('/flipkart-stream',flipkartProductAdd)


module.exports = router