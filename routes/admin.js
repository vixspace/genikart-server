const express = require("express");
const router = express.Router();
const { 
    loginAdmin,
    createAdmin,
    updateAdminPassword,
    updateAdmin,
    getAllAdmin,
    deleteAdmin,
    getVersionSetting,
    updateVersionSetting
} = require("../controller/adminController")

const {
    createCity,
    updateCity,
    fetchCity,
    deleteCity,
    createUpdateCitySetting,
    getCitySetting,
    updateCityStatus
} = require("../controller/cityController");

const {
    createPharma,
    updatePharma,
    getAllPharma,
    deletePharma,
    updatePharmaStatus
} = require("../controller/pharmaCompanyController");

const category = require("../controller/categoryController");
const sub_category = require("../controller/subCategoryController");
const heath_condition = require("../controller/heathConditionController")
const product = require("../controller/productController");
const coupon = require("../controller/couponController");
const banner = require("../controller/bannerController");
const defaultCity = require("../controller/defaultCityController");
const user = require("../controller/userController");
const order = require("../controller/orderController");



const adminToken = require("../middleware/adminToken")

// guest route
router.post('/login',loginAdmin)
router.post('/create',createAdmin);

// need token
router.put('/update-password',adminToken,updateAdminPassword);
router.put('',adminToken,updateAdmin);
router.post('/fetch',adminToken,getAllAdmin);
router.delete('/:adminId',adminToken,deleteAdmin);



router.get('/version-setting',adminToken,getVersionSetting);
router.put('/version-setting',adminToken,updateVersionSetting);

router.post('/city/create',adminToken,createCity);
router.put('/city',adminToken,updateCity);
router.put('/city/change_status',adminToken,updateCityStatus);

router.post('/city/fetch',adminToken,fetchCity);
router.delete('/city/:city_id',adminToken,deleteCity);

router.post('/city-setting',adminToken,createUpdateCitySetting);
router.get('/city-setting/:city_id',adminToken,getCitySetting);


router.post('/pharma_company/create',adminToken,createPharma);
router.put('/pharma_company',adminToken,updatePharma);
router.post('/pharma_company/fetch',adminToken,getAllPharma);
router.delete('/pharma_company/:pharma_id',adminToken,deletePharma);
router.put('/pharma_company/change_status',adminToken,updatePharmaStatus);



router.post('/category/create',adminToken,category.create);
router.put('/category',adminToken,category.update);
router.post('/category/fetch',adminToken,category.getAll);
router.delete('/category/:id',adminToken,category.remove);
router.put('/category/change_status',adminToken,category.updateStatus)



router.post('/sub_category/create',adminToken,sub_category.create);
router.put('/sub_category',adminToken,sub_category.update);
router.post('/sub_category/fetch',adminToken,sub_category.getAll);
router.delete('/sub_category/:id',adminToken,sub_category.remove);
router.put('/sub_category/change_status',adminToken,sub_category.updateStatus)



router.post('/heath_condition/create',adminToken,heath_condition.create);
router.put('/heath_condition',adminToken,heath_condition.update);
router.post('/heath_condition/fetch',adminToken,heath_condition.getAll);
router.delete('/heath_condition/:id',adminToken,heath_condition.remove);
router.put('/heath_condition/change_status',adminToken,heath_condition.updateStatus);


router.post('/product/create',adminToken,product.create);
router.put('/product',adminToken,product.update);
router.post('/product/fetch',adminToken,product.getAll);
router.delete('/product/:id',adminToken,product.remove);
router.put('/product/change_status',adminToken,product.updateStatus);
router.get('/product/:id',adminToken,product.getIndividualProduct);



router.post('/coupon/create',adminToken,coupon.create);
router.put('/coupon',adminToken,coupon.update);
router.post('/coupon/fetch',adminToken,coupon.getAll);
router.delete('/coupon/:id',adminToken,coupon.remove);
router.put('/coupon/change_status',adminToken,coupon.updateStatus);



router.post('/banner/create',adminToken,banner.create);
router.put('/banner',adminToken,banner.update);
router.post('/banner/fetch',adminToken,banner.getAll);
router.delete('/banner/:id',adminToken,banner.remove);
router.put('/banner/change_status',adminToken,banner.updateStatus);



router.put('/default_city',adminToken,defaultCity.update);
router.get('/default_city',adminToken,defaultCity.get);


router.post('/users',adminToken,user.getAll);
router.get('/users/:id',adminToken,user.getIndividualUser);
router.put('/users/update-status',adminToken,user.updateUserStatus);


router.get('/default_city',adminToken,defaultCity.get);

router.post('/orders',adminToken,order.getAll);
router.get('/orders/:order_id',adminToken,order.getEachOrder);
router.put('/orders/update-status',adminToken,order.updateStatus);
router.post('/orders/shipping',adminToken,order.shipping);
router.post('/orders/shipping-details',adminToken,order.getShippingInformation);




module.exports = router
